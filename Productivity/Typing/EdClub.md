# EdClub
https://www.edclub.com/typingclub

https://www.edclub.com/vocabulary-spelling

https://www.edclub.com/social-emotional-learning


### My Lessions
https://www.edclub.com/sportal/


## Typing course
https://www.edclub.com/typingclub/courses


## Typing Jungle

#### Home Row
https://www.edclub.com/sportal/program-3/117.play
#### Top Row
https://www.edclub.com/sportal/program-3/140.play
#### Bottom Row
https://www.edclub.com/sportal/program-3/168.play
#### Basic Level 1
https://www.edclub.com/sportal/program-3/194.play
#### Tricky Words 1
https://www.edclub.com/sportal/program-3/2976.play
#### Shift Key (Introduce using the shift keys with other keys)
https://www.edclub.com/sportal/program-3/226.play
#### Common Patterns 1
https://www.edclub.com/sportal/program-3/3002.play
#### Basic Level 2Basic Keys (Goal 30 WPM)
https://www.edclub.com/sportal/program-3/4950.play
### Tricky Words 2
https://www.edclub.com/sportal/program-3/3018.play
### NumbersIntroduce how to type numbers
https://www.edclub.com/sportal/program-3/306.play







### Current
https://www.edclub.com/sportal/program-3/118.play


## Grade-based Typing Courses New
 https://www.edclub.com/library/typing-grade1




