### environmental-ai-book

### Sea ice forecasting using IceNet

### source:
https://acocac.github.io/environmental-ai-book/polar/modelling/polar-modelling-icenet.html


1) 
        git clone https://github.com/tom-andersson/icenet-paper.git polar-modelling-icenet

2)  
        /scratch/fp0/mah900/polar-modelling-icenet-script/

3) 
        . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"


### Using environment.locked.yml
### Will have nodejs error, use the other file


### holoview 

#### From
Bokeh Version 2.2.2 (Oct 2020) 
https://docs.bokeh.org/en/latest/docs/releases.html#release-2-2-2

### Nearest
Version 1.13.5
October 23, 2020
https://holoviews.org/releases.html#version-1-13

### Added 
        /scratch/fp0/mah900/polar-modelling-icene/environmental.locked.yml
        - holoviews=1.13.5


        //mamba env create --file environment.yml
        mamba env create --file environment.locked.yml
        conda activate icenet

### hvPlot, find closest
Nov, 2020 (From description, issues)
https://hvplot.holoviz.org/releases.html#version-0-7-0

#### Conda Install
https://anaconda.org/conda-forge/hvplot

        mamba install -c conda-forge "hvplot=0.7.0"

#### Notebook
        pip install notebook

        jupyter notebook --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/scratch/fp0/mah900/polar-modelling-icenet/"


### Port forwarding
we will use port 8000 of our local machine.  
        // https://supercomputing.swin.edu.au/rcdocs/jupyter-notebooks/
        //ssh -L 8000:localhost:8888 nectar      

        REMOTE=gadi-login-02
        PORT=8888
        ssh -v -N -L $PORT:$REMOTE:$PORT gadi.nci.org.au 

### How to add your Conda environment to your jupyter notebook in just 4 steps
https://medium.com/@nrk25693/how-to-add-your-conda-environment-to-your-jupyter-notebook-in-just-4-steps-abeab8b8d084

        mamba install -c anaconda ipykernel
        python -m ipykernel install --user --name=icenet


### Holoviews
        mamba install nodejs


#### Errno 'jupyter-labextension' not found 
pip install jupyterlab
https://github.com/RoboStack/jupyter-ros/issues/4

 
#### jupyter labextension install @pyviz/jupyterlab_pyviz
https://holoviews.org/user_guide/Installing_and_Configuring.html


## 2022.09.19
## With environment.yml
        mamba env create --file environment.yml

        $ conda activate icenet

https://holoviews.org/user_guide/Installing_and_Configuring.html

        mamba install matplotlib plotly

        mamba install -c pyviz holoviews
        mamba install -c conda-forge jupyterlab

### Cannot install latest nodejs using conda on Mac
https://stackoverflow.com/questions/62325068/cannot-install-latest-nodejs-using-conda-on-mac

but all the commands that are listed in the https://anaconda.org/conda-forge/nodejs.

        mamba install -c conda-forge nodejs
        mamba install -c conda-forge/label/gcc7 nodejs
        mamba install -c conda-forge/label/cf201901 nodejs
        mamba install -c conda-forge/label/cf202003 nodejs
With this I got 13.10.1 version of nodejs. And after I did mamba update nodejs and received 14.8.0 version of it.

        jupyter labextension install @pyviz/jupyterlab_pyviz

### ValueError: The extension "@pyviz/jupyterlab_pyviz" does not yet support the current version of JupyterLab.

You no longer require the extension for JupyterLab 3.0, it'll be automatically installed if you have pyviz_comms>=2.0 installed. The docs have been updated but the website hasn't been rebuilt since.

https://github.com/holoviz/panel/issues/1965


        mamba install -c conda-forge hvplot

### Jupyter

        jupyter notebook --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/scratch/fp0/mah900/polar-modelling-icenet/"


        REMOTE=gadi-login-02
        PORT=8888
        ssh -v -N -L $PORT:$REMOTE:$PORT gadi.nci.org.au 


### Config file change 

        # data_folder = 'data'
        data_folder = '/g/data/wb00/admin/staging/icenet_1/data'

        ###############################################################################
        ### Weights and biases config (https://docs.wandb.ai/guides/track/advanced/environment-variables)
        ###############################################################################

        # Get API key from https://wandb.ai/authorize
        # WANDB_API_KEY = 'YOUR-KEY-HERE'
        WANDB_API_KEY = '2b6ea4e19e1f3527c59557cb8d69938d56e8bee0'
        # Absolute path to store wandb generated files (folder must exist)
        #   Note: user must have write access
        # WANDB_DIR = '/path/to/wandb/dir'
        WANDB_DIR = '/g/data/wb00/admin/staging/icenet_1/wandb'
        # Absolute path to wandb config dir (
        # WANDB_CONFIG_DIR = '/path/to/wandb/config/dir'
        # WANDB_CACHE_DIR = '/path/to/wandb/cache/dir'
        WANDB_CONFIG_DIR = '/g/data/wb00/admin/staging/icenet_1/.config/wandb'
        WANDB_CACHE_DIR = '/g/data/wb00/admin/staging/icenet_1/.cache/wandb,'

        ###############################################################################
        ### ECMWF details
        ###############################################################################

        # ECMWF_API_KEY = 'YOUR-KEY-HERE'
        # ECMWF_API_EMAIL = 'YOUR-KEY-HERE'
        ECMWF_API_KEY = '37096d09af3725c2147e0e96cdefd0c0'
        ECMWF_API_EMAIL = 'maruf.ahmed@anu.edu.au'



### wandb login
wandb: You can find your API key in your browser here: https://wandb.ai/authorize

### Help environment setup
https://docs.wandb.ai/guides/track/advanced/environment-variables

### ECMWF API KEY 
https://api.ecmwf.int/v1/key/ 

### ImportError: IProgress not found. Please update jupyter and ipywidgets.
https://exerror.com/importerror-intprogress-not-found-please-update-jupyter-and-ipywidgets/

        conda install -c conda-forge ipywidgets
        upyter nbextension enable --py widgetsnbextension
        Enabling notebook extension jupyter-js-widgets/extension...
              - Validating: OK



### GPU

        . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
        conda activate icenet

        jupyter lab --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/scratch/fp0/mah900/polar-modelling-icenet/"

        REMOTE=gadi-gpu-v100-0101
        PORT=8888
        ssh -v -N -L $PORT:$REMOTE:$PORT gadi.nci.org.au


### [W 2022-09-20 15:57:00.993 ServerApp] Notebook icenet/Page01.ipynb is not trusted

https://jupyter-notebook.readthedocs.io/en/stable/notebook.html#trusting-notebooks

        $ jupyter trust Page01.ipynb 
        Signing notebook: Page01.ipynb


### 2022.10.17
### For documentation

        cd /scratch/fp0/mah900/polar-modelling-icenet/icenet/
        qsub -I icenet01.pbs 

        . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
        conda activate icenet

        $ echo $CONDA_DEFAULT_ENV
        icenet

        $ echo $CONDA_PREFIX
        /scratch/fp0/mah900/Miniconda/envs/icenet

        $ jupyter trust /scratch/fp0/mah900/polar-modelling-icenet/icenet/Page02.ipynb 

        [https://stackoverflow.com/questions/36539623/how-do-i-find-the-name-of-the-conda-environment-in-which-my-code-is-running]

        jupyter lab --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/scratch/fp0/mah900/polar-modelling-icenet/icenet" 

        //CTRL + Z
        bg
        jobs         

### How do I find the name of the conda environment in which my code is running?
https://stackoverflow.com/questions/36539623/how-do-i-find-the-name-of-the-conda-environment-in-which-my-code-is-running

### Send a Process to Background Linux
https://linuxhint.com/send-process-background-linux/

##### Using CTRL + Z, bg command.
While the process is running, press CTRL + Z. This returns your shell prompt. Finally, enter the bg command to push the process in the background.

##### How to Show Background Processes
        $ jobs

##### How to Make a Process Persistent After Shell Dies
        $ nohup Firefox & 


### Local PC

        ssh -v -N -L 8888:gadi-gpu-v100-0093:8888 gadi.nci.org.au
        or,
        ssh -v -N -L 8888:gadi-login-07:8888 gadi.nci.org.au

        
        http://localhost:8888/tree/Page02.ipynb



### Try with wb00
        cp /scratch/fp0/mah900/polar-modelling-icenet/icenet/Page02.ipynb  /g/data/wb00/admin/staging/icenet_1
        cp /scratch/fp0/mah900/polar-modelling-icenet/icenet/icenet01.pbs  /g/data/wb00/admin/staging/icenet_1

        jupyter lab --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/g/data/wb00/admin/staging/icenet_1" 

### ICENET01.PBS

        #!/bin/bash

        #PBS -S /bin/bash
        #PBS -q gpuvolta
        #PBS -l ncpus=12
        #PBS -l ngpus=1
        #PBS -l jobfs=20GB
        #PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
        #PBS -l mem=20GB
        #PBS -l walltime=01:00:00
        #PBS -N IceNet01      
        #PBS -P fp0

        set +x
        . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
        conda activate icenet
        jupyter trust /scratch/fp0/mah900/polar-modelling-icenet/icenet/Page02.ipynb 

        jupyter lab --no-browser \
                --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
                --notebook-dir="/g/data/wb00/admin/staging/icenet_1" 
       
### Run
        qsub /g/data/wb00/admin/staging/icenet_1/icenet01.pbs

        ssh -v -N -L 10001:gadi-gpu-v100-0093:8888 gadi.nci.org.au
        http://localhost:10001/