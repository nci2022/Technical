### Implement the training and testing of RetinaNet from zero

#### Article catalog 

Training for RetinaNet

Testing RetinaNet on COCO datasets

Testing RetinaNet on VOC datasets

Complete training and test code

Assessment of model recurrence

https://programmer.group/implement-the-training-and-testing-of-retinanet-from-zero.html