## Command line instructions
You can also upload existing files from your computer using the instructions below.

### Git global setup
git config --global user.name "Maruf Ahmed"  <br>
git config --global user.email "nci.2022.06@gmail.com"   <br>

### Create a new repository
git clone git@gitlab.com:nci2022/technical.git   <br>
cd technical   <br>
git switch -c main   <br>
touch README.md   <br>
git add README.md   <br>
git commit -m "add README"   <br>
git push -u origin main   <br>

### Push an existing folder
cd existing_folder   <br>
git init --initial-branch=main   <br>
git remote add origin git@gitlab.com:nci2022/technical.git   <br>
git add .   <br>
git commit -m "Initial commit"   <br>
git push -u origin main   <br>

### Push an existing Git repository
cd existing_repo   <br>
git remote rename origin old-origin   <br>
git remote add origin git@gitlab.com:nci2022/technical.git   <br>
git push -u origin --all   <br>
git push -u origin --tags   <br>
