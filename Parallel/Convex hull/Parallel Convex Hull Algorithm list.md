1. CONVEX HULL - PARALLEL AND DISTRIBUTED ALGORITHMS @stanford.edu

https://stanford.edu/~rezab/classes/cme323/S16/projects_reports/suresha_ramesh.pdf

1. Efficient Parallel Convex Hull Algorithms 

https://web.eecs.umich.edu/~qstout/abs/IEEETC88a.html

1. Parallel Algorithms for Constructing Convex Hulls. 

https://digitalcommons.lsu.edu/cgi/viewcontent.cgi?article=7028&context=gradschool_disstheses

1. A FAST PARALLEL ALGORITHM FOR FINDING THE CONVEX HULL OF A SORTED POINT SET

https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.53.8151&rep=rep1&type=pdf

1. https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.53.8151&rep=rep1&type=pdf

https://www.maplesoft.com/Applications/Detail.aspx?id=103785

1. A Simple Parallel Convex Hulls Algorithm for Sorted Points and the Performance Evaluation on the Multicore Processors

https://www.researchgate.net/publication/221396615_A_Simple_Parallel_Convex_Hulls_Algorithm_for_Sorted_Points_and_the_Performance_Evaluation_on_the_Multicore_Processors

1. Solving 2D Convex Hull With Parallel Algorithms

http://service.scs.carleton.ca/sites/default/files/honours_projects/2015/4905%20report.pdf




