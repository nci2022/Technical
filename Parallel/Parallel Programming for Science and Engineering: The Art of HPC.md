### Parallel Programming for Science and Engineering
### The Art of HPC, volume 2
### Victor Eijkhout

https://web.corral.tacc.utexas.edu/CompEdu/pdf/pcse/EijkhoutParallelProgramming.pdf