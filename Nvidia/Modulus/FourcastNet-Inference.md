
### Inference:
https://github.com/NVlabs/FourCastNet#inference

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
## Download NVlabs fourcastnet
    cd /scratch/fp0/mah900/
    git clone https://github.com/NVlabs/FourCastNet.git

## Run personal endpoint
### First login to the previous node
    ssh gadi-login-08

    /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -status
    /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -stop

### To launch Globus Connect Personal in CLI mode, use
    nohup /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -start &

    $ /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -status
    Globus Online:   connected
    Transfer Status: idle

## 1. The path to the out of training sample hdf5 file. 
It could be out_of_sample dataset hosted here (https://app.globus.org/file-manager?origin_id=945b3c9e-0f8c-11ed-8daf-9f359c660fbd&origin_path=%2F~%2Fdata%2F). 

The inference script provided assumes that you are using the `out_of_sample/2018.h5` file.

You can modify the script to use a different h5 file that you processed yourself after downloading the raw data from Copernicus.

    fourcast_ep=945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    my_ep=85a9f386-5f53-11ed-b0b5-bfe7e7197080   
### Test
    globus ls ${fourcast_ep}:/~/data/FCN_ERA5_data_v0/out_of_sample/
    globus ls ${my_ep}:/scratch/fp0/mah900/globus_data/      
### Transfer
globus transfer ${fourcast_ep}:/~/data/FCN_ERA5_data_v0/out_of_sample/ ${my_ep}:/scratch/fp0/mah900/globus_data/ --label Sample_Data -r -s checksum --verify-checksum

    Message: The transfer has been accepted and a task has been created and queued for execution
    Task ID: 1a26da58-5fde-11ed-8421-8d2923bcb3ec

(/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 ~]$ globus task show  1a26da58-5fde-11ed-8421-8d2923bcb3ec

    Label:                        Sample_Data
    Task ID:                      1a26da58-5fde-11ed-8421-8d2923bcb3ec
    Is Paused:                    False
    Type:                         TRANSFER
    Directories:                  1
    Files:                        1
    Status:                       SUCCEEDED
    Request Time:                 2022-11-09T03:25:09+00:00
    Faults:                       0
    Total Subtasks:               3
    Subtasks Succeeded:           3
    Subtasks Pending:             0
    Subtasks Retrying:            0
    Subtasks Failed:              0
    Subtasks Canceled:            0
    Subtasks Expired:             0
    Subtasks with Skipped Errors: 0
    Completion Time:              2022-11-09T05:25:51+00:00
    Source Endpoint:              FourCastNet
    Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    Destination Endpoint:         my_ep1
    Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
    Bytes Transferred:            127329755000
    Bytes Per Second:             17582264




=====================================================================================    

## 2. The model weights hosted at Trained Model Weights
https://app.globus.org/file-manager?origin_id=945b3c9e-0f8c-11ed-8daf-9f359c660fbd&origin_path=%2F~%2Fmodel_weights%2F


    globus ls ${fourcast_ep}:/~/model_weights/

globus transfer ${fourcast_ep}:/~/model_weights/ ${my_ep}:/scratch/fp0/mah900/globus_data/ --label Trained_Model_Weights -r -s checksum --verify-checksum

    Message: The transfer has been accepted and a task has been created and queued for execution
    Task ID: 926a5912-5fe4-11ed-8fd1-e9cb7c15c7d2

    (/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 ~]$ globus task show 926a5912-5fe4-11ed-8fd1-e9cb7c15c7d2
    Label:                        Trained_Model_Weights
    Task ID:                      926a5912-5fe4-11ed-8fd1-e9cb7c15c7d2
    Is Paused:                    False
    Type:                         TRANSFER
    Directories:                  2
    Files:                        2
    Status:                       SUCCEEDED
    Request Time:                 2022-11-09T04:11:27+00:00
    Faults:                       0
    Total Subtasks:               5
    Subtasks Succeeded:           5
    Subtasks Pending:             0
    Subtasks Retrying:            0
    Subtasks Failed:              0
    Subtasks Canceled:            0
    Subtasks Expired:             0
    Subtasks with Skipped Errors: 0
    Completion Time:              2022-11-09T04:13:03+00:00
    Source Endpoint:              FourCastNet
    Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    Destination Endpoint:         my_ep1
    Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
    Bytes Transferred:            1781659006
    Bytes Per Second:             18743257

=====================================================================================

## 3. The pre-computed normalization statistics hosted at additional. 
https://app.globus.org/file-manager?origin_id=945b3c9e-0f8c-11ed-8daf-9f359c660fbd&origin_path=%2F~%2Fadditional%2F

    (/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 ~]$ globus ls ${fourcast_ep}:/~/additional/
    stats_v0/

(/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 ~]$ globus transfer ${fourcast_ep}:/~/additional/ ${my_ep}:/scratch/fp0/mah900/globus_data/ --label additional -r -s checksum --verify-checksum
Message: The transfer has been accepted and a task has been created and queued for execution
Task ID: e1666798-5fe6-11ed-8fd1-e9cb7c15c7d2

(/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 FourCastNet]$ globus task show e1666798-5fe6-11ed-8fd1-e9cb7c15c7d2
    Label:                        additional
    Task ID:                      e1666798-5fe6-11ed-8fd1-e9cb7c15c7d2
    Is Paused:                    False
    Type:                         TRANSFER
    Directories:                  3
    Files:                        8
    Status:                       SUCCEEDED
    Request Time:                 2022-11-09T04:27:59+00:00
    Faults:                       1
    Total Subtasks:               12
    Subtasks Succeeded:           12
    Subtasks Pending:             0
    Subtasks Retrying:            0
    Subtasks Failed:              0
    Subtasks Canceled:            0
    Subtasks Expired:             0
    Subtasks with Skipped Errors: 0
    Completion Time:              2022-11-09T06:36:16+00:00
    Source Endpoint:              FourCastNet
    Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    Destination Endpoint:         my_ep1
    Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
    Bytes Transferred:            127520789516
    Bytes Per Second:             16567778




=======================================================================================

# Next, Install
https://github.com/NVlabs/FourCastNet/blob/master/docker/Dockerfile

# install mpi4py
pip install mpi4py

      _configtest.c:2:10: fatal error: mpi.h: No such file or directory
       #include <mpi.h>
                ^~~~~~~
      compilation terminated.
      failure.
      removing: _configtest.c _configtest.o
      error: Cannot compile MPI programs. Check your configuration!!!
      [end of output]
#### fatal error: mpi.h: No such file or directory #114
For RedHat and CentOS:

    yum install openmpi-devel
    export CC=/usr/lib64/openmpi/bin/mpicc
    pip install mpi4py
https://github.com/openai/baselines/issues/114

### Solution 
$ module load openmpi/4.1.4
$ pip install mpi4py

# h5py
pip install h5py

    Looking in indexes: https://pypi.org/simple, https://pypi.ngc.nvidia.com
    Requirement already satisfied: h5py in /scratch/fp0/mah900/env/fourcastnet/lib/python3.8/site-packages (3.7.0)
    Requirement already satisfied: numpy>=1.14.5 in /scratch/fp0/mah900/env/fourcastnet/lib/python3.8/site-packages (from h5py) (1.23.3)


# other python stuff
pip install wandb  
pip install ruamel.yaml  
pip install --upgrade tqdm  
pip install timm  
    Requirement already satisfied:
pip install einops
    Requirement already satisfied:

# benchy
pip install git+https://github.com/romerojosh/benchy.git



=======================================================================================
## Test, from
https://github.com/NVlabs/FourCastNet/blob/master/inference/inference.py

import os
import sys
import time
import numpy as np
import argparse
##################
# sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')
###################
from numpy.core.numeric import False_
import h5py
import torch
import torchvision
from torchvision.utils import save_image
import torch.nn as nn
import torch.cuda.amp as amp
import torch.distributed as dist
from collections import OrderedDict
from torch.nn.parallel import DistributedDataParallel
import logging
from utils import logging_utils
from utils.weighted_acc_rmse import weighted_rmse_torch_channels, weighted_acc_torch_channels, unweighted_acc_torch_channels, weighted_acc_masked_torch_channels
logging_utils.config_logger()
from utils.YParams import YParams
from utils.data_loader_multifiles import get_data_loader
from networks.afnonet import AFNONet
#####################################
import wandb
import matplotlib.pyplot as plt
import glob
from datetime import datetime

=======================================================================================

## Configure config/AFNO.yaml,
Once you have all the file listed above
In config/AFNO.yaml (https://github.com/NVlabs/FourCastNet/blob/master/config/AFNO.yaml), set the user defined paths

##### Additional information on batched ensemble inference and precipitation model inference
can be found at inference/README_inference.md.
(https://github.com/NVlabs/FourCastNet/blob/master/inference/README_inference.md)

##### nano -c +62 /scratch/fp0/mah900/FourCastNet/config/AFNO.yaml
Updated

    afno_backbone: &backbone
      <<: *FULL_FIELD
      log_to_wandb: !!bool True
      lr: 5E-4
      batch_size: 64
      max_epochs: 150
      scheduler: 'CosineAnnealingLR'
      in_channels: [0, 1 ,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
      out_channels: [0, 1 ,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
      orography: !!bool False
      orography_path: None
      exp_dir: '/pscratch/sd/s/shas1693/results/era5_wind'
      train_data_path: '/pscratch/sd/s/shas1693/data/era5/train'
      valid_data_path: '/pscratch/sd/s/shas1693/data/era5/test'
      inf_data_path:  '/scratch/fp0/mah900/globus_data/out_of_sample' # '/pscratch/sd/s/shas1693/data/era5/out_of_sample'
      time_means_path:   '/scratch/fp0/mah900/globus_data/stats_v0/time_means.npy'   # '/pscratch/sd/s/shas1693/data/era5/time_means.npy'
      global_means_path: '/scratch/fp0/mah900/globus_data/stats_v0/global_means.npy' #  '/pscratch/sd/s/shas1693/data/era5/global_means.npy'
      global_stds_path:  '/scratch/fp0/mah900/globus_data/stats_v0/global_stds.npy'  # '/pscratch/sd/s/shas1693/data/era5/global_stds.npy'

==========================================================================================

## Get GPU
    qsub -I /home/900/mah900/conda/g1_h2.pbs 
    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    module load openmpi/4.1.4

## Run (1st, Fail)

    cd /scratch/fp0/mah900/FourCastNet
    (/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 FourCastNet]$ \
    python inference/inference.py --config=afno_backbone --run_num=0 \
        --weights '/scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt' \
        --override_dir '/scratch/fp0/mah900/FourCastNet/output/ \' 

=====================================================================================
# Output

2022-11-09 19:29:50,412 - root - INFO - --------------- Versions ---------------
2022-11-09 19:29:50,495 - root - INFO - git branch: b'* master'
2022-11-09 19:29:50,521 - root - INFO - git hash: b'4cac4678bcf003e2692f3c6528ccb4aa62204f4d'
2022-11-09 19:29:50,521 - root - INFO - Torch: 1.13.0
2022-11-09 19:29:50,521 - root - INFO - ----------------------------------------
2022-11-09 19:29:50,521 - root - INFO - ------------------ Configuration ------------------
2022-11-09 19:29:50,521 - root - INFO - Configuration file: /scratch/fp0/mah900/FourCastNet/config/AFNO.yaml
2022-11-09 19:29:50,522 - root - INFO - Configuration name: afno_backbone
2022-11-09 19:29:50,522 - root - INFO - log_to_wandb True
2022-11-09 19:29:50,522 - root - INFO - lr 0.0005
2022-11-09 19:29:50,522 - root - INFO - batch_size 64
2022-11-09 19:29:50,522 - root - INFO - max_epochs 150
2022-11-09 19:29:50,522 - root - INFO - scheduler CosineAnnealingLR
2022-11-09 19:29:50,522 - root - INFO - in_channels [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
2022-11-09 19:29:50,522 - root - INFO - out_channels [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
2022-11-09 19:29:50,522 - root - INFO - orography False
2022-11-09 19:29:50,523 - root - INFO - orography_path None
2022-11-09 19:29:50,523 - root - INFO - exp_dir /pscratch/sd/s/shas1693/results/era5_wind
2022-11-09 19:29:50,523 - root - INFO - train_data_path /pscratch/sd/s/shas1693/data/era5/train
2022-11-09 19:29:50,523 - root - INFO - valid_data_path /pscratch/sd/s/shas1693/data/era5/test
2022-11-09 19:29:50,523 - root - INFO - inf_data_path /scratch/fp0/mah900/globus_data/out_of_sample
2022-11-09 19:29:50,523 - root - INFO - time_means_path /scratch/fp0/mah900/globus_data/stats_v0/time_means.npy
2022-11-09 19:29:50,523 - root - INFO - global_means_path /scratch/fp0/mah900/globus_data/stats_v0/global_means.npy
2022-11-09 19:29:50,523 - root - INFO - global_stds_path /scratch/fp0/mah900/globus_data/stats_v0/global_stds.npy
2022-11-09 19:29:50,523 - root - INFO - loss l2
2022-11-09 19:29:50,523 - root - INFO - num_data_workers 4
2022-11-09 19:29:50,524 - root - INFO - dt 1
2022-11-09 19:29:50,524 - root - INFO - n_history 0
2022-11-09 19:29:50,524 - root - INFO - prediction_type iterative
2022-11-09 19:29:50,524 - root - INFO - prediction_length 41
2022-11-09 19:29:50,524 - root - INFO - n_initial_conditions 5
2022-11-09 19:29:50,524 - root - INFO - ics_type default
2022-11-09 19:29:50,524 - root - INFO - save_raw_forecasts True
2022-11-09 19:29:50,524 - root - INFO - save_channel False
2022-11-09 19:29:50,524 - root - INFO - masked_acc False
2022-11-09 19:29:50,524 - root - INFO - maskpath None
2022-11-09 19:29:50,525 - root - INFO - perturb False
2022-11-09 19:29:50,525 - root - INFO - add_grid False
2022-11-09 19:29:50,525 - root - INFO - N_grid_channels 0
2022-11-09 19:29:50,525 - root - INFO - gridtype sinusoidal
2022-11-09 19:29:50,525 - root - INFO - roll False
2022-11-09 19:29:50,525 - root - INFO - num_blocks 8
2022-11-09 19:29:50,525 - root - INFO - nettype afno
2022-11-09 19:29:50,525 - root - INFO - patch_size 8
2022-11-09 19:29:50,525 - root - INFO - width 56
2022-11-09 19:29:50,525 - root - INFO - modes 32
2022-11-09 19:29:50,526 - root - INFO - target default
2022-11-09 19:29:50,526 - root - INFO - normalization zscore
2022-11-09 19:29:50,526 - root - INFO - log_to_screen True
2022-11-09 19:29:50,526 - root - INFO - save_checkpoint True
2022-11-09 19:29:50,526 - root - INFO - enable_nhwc False
2022-11-09 19:29:50,526 - root - INFO - optimizer_type FusedAdam
2022-11-09 19:29:50,526 - root - INFO - crop_size_x None
2022-11-09 19:29:50,526 - root - INFO - crop_size_y None
2022-11-09 19:29:50,526 - root - INFO - two_step_training False
2022-11-09 19:29:50,526 - root - INFO - plot_animations False
2022-11-09 19:29:50,526 - root - INFO - add_noise False
2022-11-09 19:29:50,527 - root - INFO - noise_std 0
2022-11-09 19:29:50,527 - root - INFO - world_size 1
2022-11-09 19:29:50,527 - root - INFO - interp 0
2022-11-09 19:29:50,527 - root - INFO - use_daily_climatology False
2022-11-09 19:29:50,527 - root - INFO - global_batch_size 64
2022-11-09 19:29:50,527 - root - INFO - experiment_dir /scratch/fp0/mah900/FourCastNet/output/ \
2022-11-09 19:29:50,527 - root - INFO - best_checkpoint_path /scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt
2022-11-09 19:29:50,527 - root - INFO - resuming False
2022-11-09 19:29:50,527 - root - INFO - local_rank 0
2022-11-09 19:29:50,527 - root - INFO - ---------------------------------------------------
2022-11-09 19:29:50,528 - root - INFO - Inference for 36 initial conditions
2022-11-09 19:29:50,643 - root - INFO - Getting file stats from /scratch/fp0/mah900/globus_data/out_of_sample/2018.h5
2022-11-09 19:29:50,741 - root - INFO - Number of samples per year: 1460
2022-11-09 19:29:50,741 - root - INFO - Found data at path /scratch/fp0/mah900/globus_data/out_of_sample. Number of examples: 1460. Image Shape: 720 x 1440 x 20
2022-11-09 19:29:50,741 - root - INFO - Delta t: 6 hours
2022-11-09 19:29:50,742 - root - INFO - Including 0 hours of past history in training at a frequency of 6 hours
2022-11-09 19:29:50,743 - root - INFO - Loading trained model checkpoint from /scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt
2022-11-09 19:30:04,241 - root - INFO - Loading inference data
2022-11-09 19:30:04,241 - root - INFO - Inference data from /scratch/fp0/mah900/globus_data/out_of_sample/2018.h5
2022-11-09 19:30:04,243 - root - INFO - Initial condition 1 of 36
2022-11-09 19:30:27,928 - root - INFO - Begin autoregressive inference
2022-11-09 19:30:35,381 - root - INFO - Predicted timestep 0 of 41. z500 RMS Error: 0.0, ACC: 1.0
2022-11-09 19:30:37,103 - root - INFO - Predicted timestep 1 of 41. z500 RMS Error: 34.42151641845703, ACC: 0.9996494054794312

...

2022-11-09 19:55:27,081 - root - INFO - Predicted timestep 28 of 41. z500 RMS Error: 767.99072265625, ACC: 0.7489241361618042
2022-11-09 19:55:27,307 - root - INFO - Predicted timestep 29 of 41. z500 RMS Error: 796.9030151367188, ACC: 0.7336283922195435
2022-11-09 19:55:27,534 - root - INFO - Predicted timestep 30 of 41. z500 RMS Error: 837.1039428710938, ACC: 0.7045760750770569
2022-11-09 19:55:27,760 - root - INFO - Predicted timestep 31 of 41. z500 RMS Error: 873.6126708984375, ACC: 0.6827325224876404
2022-11-09 19:55:27,987 - root - INFO - Predicted timestep 32 of 41. z500 RMS Error: 915.8746337890625, ACC: 0.6518777012825012
2022-11-09 19:55:28,213 - root - INFO - Predicted timestep 33 of 41. z500 RMS Error: 953.3584594726562, ACC: 0.6292368769645691
2022-11-09 19:55:28,440 - root - INFO - Predicted timestep 34 of 41. z500 RMS Error: 983.1431884765625, ACC: 0.6032803058624268
2022-11-09 19:55:28,666 - root - INFO - Predicted timestep 35 of 41. z500 RMS Error: 999.49072265625, ACC: 0.5942789316177368
2022-11-09 19:55:28,893 - root - INFO - Predicted timestep 36 of 41. z500 RMS Error: 1007.3109130859375, ACC: 0.5899479389190674
2022-11-09 19:55:29,119 - root - INFO - Predicted timestep 37 of 41. z500 RMS Error: 1003.2991943359375, ACC: 0.5981574058532715
2022-11-09 19:55:29,346 - root - INFO - Predicted timestep 38 of 41. z500 RMS Error: 998.961669921875, ACC: 0.600583553314209
2022-11-09 19:55:29,572 - root - INFO - Predicted timestep 39 of 41. z500 RMS Error: 1002.5892333984375, ACC: 0.6022067070007324
2022-11-09 19:55:29,798 - root - INFO - Predicted timestep 40 of 41. z500 RMS Error: 1014.1946411132812, ACC: 0.597285270690918
2022-11-09 19:55:38,286 - root - INFO - Saving files at /scratch/fp0/mahls 900/FourCastNet/output/ \/autoregressive_predictions_z500.h5




==========================================================================================

## Run (2nd)

    cd /scratch/fp0/mah900/FourCastNet
    (/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 FourCastNet]$ \
    python inference/inference.py --config=afno_backbone --run_num=0 \
        --weights '/scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt' \
        --override_dir '/scratch/fp0/mah900/globus_data/output' 

=============================================================================================
# Output

*** params['experiment_dir']= /scratch/fp0/mah900/globus_data/output
2022-11-09 21:52:55,894 - root - INFO - --------------- Versions ---------------
2022-11-09 21:52:55,939 - root - INFO - git branch: b'* master'
2022-11-09 21:52:55,978 - root - INFO - git hash: b'4cac4678bcf003e2692f3c6528ccb4aa62204f4d'
2022-11-09 21:52:55,978 - root - INFO - Torch: 1.13.0
2022-11-09 21:52:55,978 - root - INFO - ----------------------------------------
2022-11-09 21:52:55,979 - root - INFO - ------------------ Configuration ------------------
2022-11-09 21:52:55,979 - root - INFO - Configuration file: /scratch/fp0/mah900/FourCastNet/config/AFNO.yaml
2022-11-09 21:52:55,979 - root - INFO - Configuration name: afno_backbone
2022-11-09 21:52:55,979 - root - INFO - log_to_wandb True
2022-11-09 21:52:55,979 - root - INFO - lr 0.0005
2022-11-09 21:52:55,979 - root - INFO - batch_size 64
2022-11-09 21:52:55,979 - root - INFO - max_epochs 150
2022-11-09 21:52:55,979 - root - INFO - scheduler CosineAnnealingLR
2022-11-09 21:52:55,979 - root - INFO - in_channels [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
2022-11-09 21:52:55,980 - root - INFO - out_channels [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
2022-11-09 21:52:55,980 - root - INFO - orography False
2022-11-09 21:52:55,980 - root - INFO - orography_path None
2022-11-09 21:52:55,980 - root - INFO - exp_dir /pscratch/sd/s/shas1693/results/era5_wind
2022-11-09 21:52:55,980 - root - INFO - train_data_path /pscratch/sd/s/shas1693/data/era5/train
2022-11-09 21:52:55,980 - root - INFO - valid_data_path /pscratch/sd/s/shas1693/data/era5/test
2022-11-09 21:52:55,980 - root - INFO - inf_data_path /scratch/fp0/mah900/globus_data/out_of_sample
2022-11-09 21:52:55,980 - root - INFO - time_means_path /scratch/fp0/mah900/globus_data/stats_v0/time_means.npy
2022-11-09 21:52:55,980 - root - INFO - global_means_path /scratch/fp0/mah900/globus_data/stats_v0/global_means.npy
2022-11-09 21:52:55,981 - root - INFO - global_stds_path /scratch/fp0/mah900/globus_data/stats_v0/global_stds.npy
2022-11-09 21:52:55,981 - root - INFO - loss l2
2022-11-09 21:52:55,981 - root - INFO - num_data_workers 4
2022-11-09 21:52:55,981 - root - INFO - dt 1
2022-11-09 21:52:55,981 - root - INFO - n_history 0
2022-11-09 21:52:55,981 - root - INFO - prediction_type iterative
2022-11-09 21:52:55,981 - root - INFO - prediction_length 41
2022-11-09 21:52:55,981 - root - INFO - n_initial_conditions 5
2022-11-09 21:52:55,981 - root - INFO - ics_type default
2022-11-09 21:52:55,981 - root - INFO - save_raw_forecasts True
2022-11-09 21:52:55,982 - root - INFO - save_channel False
2022-11-09 21:52:55,982 - root - INFO - masked_acc False
2022-11-09 21:52:55,982 - root - INFO - maskpath None
2022-11-09 21:52:55,982 - root - INFO - perturb False
2022-11-09 21:52:55,982 - root - INFO - add_grid False
2022-11-09 21:52:55,982 - root - INFO - N_grid_channels 0
2022-11-09 21:52:55,982 - root - INFO - gridtype sinusoidal
2022-11-09 21:52:55,982 - root - INFO - roll False
2022-11-09 21:52:55,982 - root - INFO - num_blocks 8
2022-11-09 21:52:55,983 - root - INFO - nettype afno
2022-11-09 21:52:55,983 - root - INFO - patch_size 8
2022-11-09 21:52:55,983 - root - INFO - width 56
2022-11-09 21:52:55,983 - root - INFO - modes 32
2022-11-09 21:52:55,983 - root - INFO - target default
2022-11-09 21:52:55,983 - root - INFO - normalization zscore
2022-11-09 21:52:55,983 - root - INFO - log_to_screen True
2022-11-09 21:52:55,983 - root - INFO - save_checkpoint True
2022-11-09 21:52:55,983 - root - INFO - enable_nhwc False
2022-11-09 21:52:55,983 - root - INFO - optimizer_type FusedAdam
2022-11-09 21:52:55,984 - root - INFO - crop_size_x None
2022-11-09 21:52:55,984 - root - INFO - crop_size_y None
2022-11-09 21:52:55,984 - root - INFO - two_step_training False
2022-11-09 21:52:55,984 - root - INFO - plot_animations False
2022-11-09 21:52:55,984 - root - INFO - add_noise False
2022-11-09 21:52:55,984 - root - INFO - noise_std 0
2022-11-09 21:52:55,984 - root - INFO - world_size 1
2022-11-09 21:52:55,984 - root - INFO - interp 0
2022-11-09 21:52:55,984 - root - INFO - use_daily_climatology False
2022-11-09 21:52:55,984 - root - INFO - global_batch_size 64
2022-11-09 21:52:55,985 - root - INFO - experiment_dir /scratch/fp0/mah900/globus_data/output
2022-11-09 21:52:55,985 - root - INFO - best_checkpoint_path /scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt
2022-11-09 21:52:55,985 - root - INFO - resuming False
2022-11-09 21:52:55,985 - root - INFO - local_rank 0
2022-11-09 21:52:55,985 - root - INFO - ---------------------------------------------------
2022-11-09 21:52:55,985 - root - INFO - Inference for 36 initial conditions
2022-11-09 21:52:56,085 - root - INFO - Getting file stats from /scratch/fp0/mah900/globus_data/out_of_sample/2018.h5
2022-11-09 21:52:56,239 - root - INFO - Number of samples per year: 1460
2022-11-09 21:52:56,239 - root - INFO - Found data at path /scratch/fp0/mah900/globus_data/out_of_sample. Number of examples: 1460. Image Shape: 720 x 1440 x 20
2022-11-09 21:52:56,240 - root - INFO - Delta t: 6 hours
2022-11-09 21:52:56,240 - root - INFO - Including 0 hours of past history in training at a frequency of 6 hours
2022-11-09 21:52:56,241 - root - INFO - Loading trained model checkpoint from /scratch/fp0/mah900/globus_data/FCN_weights_v0/backbone.ckpt
2022-11-09 21:53:10,304 - root - INFO - Loading inference data
2022-11-09 21:53:10,305 - root - INFO - Inference data from /scratch/fp0/mah900/globus_data/out_of_sample/2018.h5
2022-11-09 21:53:10,307 - root - INFO - Initial condition 1 of 36
2022-11-09 21:53:35,182 - root - INFO - Begin autoregressive inference
2022-11-09 21:53:44,220 - root - INFO - Predicted timestep 0 of 41. z500 RMS Error: 0.0, ACC: 1.0
2022-11-09 21:53:45,899 - root - INFO - Predicted timestep 1 of 41. z500 RMS Error: 34.42151641845703, ACC: 0.9996494054794312
2022-11-09 21:53:46,124 - root - INFO - Predicted timestep 2 of 41. z500 RMS Error: 48.36748123168945, ACC: 0.9993204474449158

...

2022-11-09 22:15:28,125 - root - INFO - Predicted timestep 28 of 41. z500 RMS Error: 767.99072265625, ACC: 0.7489241361618042
2022-11-09 22:15:28,351 - root - INFO - Predicted timestep 29 of 41. z500 RMS Error: 796.9030151367188, ACC: 0.7336283922195435
2022-11-09 22:15:28,578 - root - INFO - Predicted timestep 30 of 41. z500 RMS Error: 837.1039428710938, ACC: 0.7045760750770569
2022-11-09 22:15:28,804 - root - INFO - Predicted timestep 31 of 41. z500 RMS Error: 873.6126708984375, ACC: 0.6827325224876404
2022-11-09 22:15:29,030 - root - INFO - Predicted timestep 32 of 41. z500 RMS Error: 915.8746337890625, ACC: 0.6518777012825012
2022-11-09 22:15:29,257 - root - INFO - Predicted timestep 33 of 41. z500 RMS Error: 953.3584594726562, ACC: 0.6292368769645691
2022-11-09 22:15:29,483 - root - INFO - Predicted timestep 34 of 41. z500 RMS Error: 983.1431884765625, ACC: 0.6032803058624268
2022-11-09 22:15:29,709 - root - INFO - Predicted timestep 35 of 41. z500 RMS Error: 999.49072265625, ACC: 0.5942789316177368
2022-11-09 22:15:29,935 - root - INFO - Predicted timestep 36 of 41. z500 RMS Error: 1007.3109130859375, ACC: 0.5899479389190674
2022-11-09 22:15:30,162 - root - INFO - Predicted timestep 37 of 41. z500 RMS Error: 1003.2991943359375, ACC: 0.5981574058532715
2022-11-09 22:15:30,388 - root - INFO - Predicted timestep 38 of 41. z500 RMS Error: 998.961669921875, ACC: 0.600583553314209
2022-11-09 22:15:30,614 - root - INFO - Predicted timestep 39 of 41. z500 RMS Error: 1002.5892333984375, ACC: 0.6022067070007324
2022-11-09 22:15:30,840 - root - INFO - Predicted timestep 40 of 41. z500 RMS Error: 1014.1946411132812, ACC: 0.597285270690918
2022-11-09 22:15:36,256 - root - INFO - Saving files at /scratch/fp0/mah900/globus_data/output/autoregressive_predictions_z500.h5


==================================================================================================================================

-rw-r--r-- 1 mah900 fp0 696K Nov  9 22:15 /scratch/fp0/mah900/globus_data/output/autoregressive_predictions_z500.h5


## ARE

### Jupyter command `jupyter-lab` not found
https://stackoverflow.com/questions/57677481/jupyter-command-jupyter-lab-not-found

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    python -m pip install jupyterlab

    $ jupyter-lab --version
    3.5.0

### Install jupyterlab-h5web
https://github.com/silx-kit/jupyterlab-h5web

    pip install jupyterlab_h5web


### Launch

1. Queue: gpuvolta

1. Compute Size: 1gpu

1. Project: fp0

1. Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

1. Advanced options

1. Modules: openmpi/4.1.4

1. Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda/

1. Conda environment: /scratch/fp0/mah900/env/fourcastnet

====================================================================

Jupyter: /scratch/fp0/mah900/env/fourcastnet/bin/jupyter

File -> Open From Paht -> /scratch/fp0/mah900/FourCastNet/FourCastNet-01.ipynb

### Test 
%cd /scratch/fp0/mah900/FourCastNet
    /scratch/fp0/mah900/FourCastNet

!which python
    /scratch/fp0/mah900/env/fourcastnet/bin/python

Then, From Above, added Try Catch

### Error in Jupyter interactive. 
Javascript Error: IPython is not defined

### Install ipymal with PIP (Did not work)
https://stackoverflow.com/questions/51922480/javascript-error-ipython-is-not-defined-in-jupyterlab

    pip install ipympl  
    # If using JupyterLab 2
    pip install nodejs
### Error
    $ jupyter labextension install @jupyter-widgets/jupyterlab-manager
    An error occurred.
    ValueError: Please install Node.js and npm before continuing installation. You may be able to install Node.js from your package manager, from conda, or directly from the Node.js website (https://nodejs.org).

 

### Install ipyml with conda
https://stackoverflow.com/questions/51922480/javascript-error-ipython-is-not-defined-in-jupyterlab


    ## Using conda
    conda install -c conda-forge ipympl

    # If using JupyterLab 2
    conda install nodejs
    jupyter labextension install @jupyter-widgets/jupyterlab-manager
    jupyter lab build

    # Later, if updating a previous Lab install:
    conda install ipympl
    jupyter lab build

### Color tried
    color_map ='PuBuGn'
    ###color_map = 'afmhot'
    #color_map = 'cubehelix'
    ##color_map = 'gist_earth'  
    #color_map = 'gist_stern'
    #color_map = 'gist_stern_r'
    #color_map = 'magma'
    #color_map = 'nipy_spectral_r'
    #color_map = 'plasma'


## Visualization

### 1. 
filename = '/scratch/fp0/mah900/globus_data/output/autoregressive_predictions_z500_vis.h5'
#pred=""
with h5py.File(filename, 'r') as f1:
    datasetNames = [n for n in f1.keys()]
    for n in datasetNames:
        print(n,":")
        dset = f1[n]
        print(" ", dset.shape,":",dset.dtype)
    global pred
    pred = f1['predicted'][()]    
    
print ( pred[0][0][0].shape)    


### 2. 

from matplotlib import pyplot as plt
plt.rcParams["figure.figsize"] = [7.0, 3.50]
plt.rcParams["figure.autolayout"] = True
#for i in range (41):
#--plt.imshow(pred[0][i][0], interpolation='nearest')
#--plt.show()
plt.imshow(pred[0][1][2], interpolation='nearest')
plt.show()



 ##  inference for precipitation: data download

   . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet

    fourcast_ep=945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    my_ep=85a9f386-5f53-11ed-b0b5-bfe7e7197080   

    globus ls ${fourcast_ep}:/data/FCN_ERA5_data_v0/precip/out_of_sample/
    globus ls ${my_ep}:/scratch/fp0/mah900/globus_data/precip/out_of_sample/      
 
    globus transfer ${fourcast_ep}:/data/FCN_ERA5_data_v0/precip/out_of_sample/ ${my_ep}:/scratch/fp0/mah900/globus_data/precip/out_of_sample/ --label Precipitation_Data -r -s checksum --verify-checksum

    Message: The transfer has been accepted and a task has been created and queued for execution
    Task ID: ef034e50-6236-11ed-8fd1-e9cb7c15c7d2

    globus task show ef034e50-6236-11ed-8fd1-e9cb7c15c7d2

    Label:                        Precipitation_Data
    Task ID:                      ef034e50-6236-11ed-8fd1-e9cb7c15c7d2
    Is Paused:                    False
    Type:                         TRANSFER
    Directories:                  1
    Files:                        2
    Status:                       SUCCEEDED
    Request Time:                 2022-11-12T03:06:04+00:00
    Faults:                       0
    Total Subtasks:               4
    Subtasks Succeeded:           4
    Subtasks Pending:             0
    Subtasks Retrying:            0
    Subtasks Failed:              0
    Subtasks Canceled:            0
    Subtasks Expired:             0
    Subtasks with Skipped Errors: 0
    Completion Time:              2022-11-12T03:07:51+00:00
    Source Endpoint:              FourCastNet
    Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    Destination Endpoint:         my_ep1
    Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
    Bytes Transferred:            12109828096
    Bytes Per Second:             112824825

===================================================================


##  inference for precipitation: script


cd /scratch/fp0/mah900/FourCastNet
(/scratch/fp0/mah900/env/fourcastnet) [mah900@gadi-login-04 FourCastNet]$ \
python inference/inference_precip.py --config=precip --run_num=0 --vis \
        --weights '/scratch/fp0/mah900/globus_data/FCN_weights_v0/precip.ckpt' \
        --override_dir '/scratch/fp0/mah900/globus_data/output' 

### Visualization

### 1. 
filename = '/scratch/fp0/mah900/globus_data/output/autoregressive_predictions_tp.h5'
#pred=""
with h5py.File(filename, 'r') as f1:
    datasetNames = [n for n in f1.keys()]
    for n in datasetNames:
        print(n,":")
        dset = f1[n]
        print(" ", dset.shape,":",dset.dtype)
    global pred
    pred = f1['predicted'][()]    
    
print ( pred[0][0][0].shape) 

### 2.1

%matplotlib widget 
import numpy as np
from matplotlib import pyplot as plt_2
from matplotlib.widgets import Slider
import time

filename = '/scratch/fp0/mah900/globus_data/output/autoregressive_predictions_tp.h5'
with h5py.File(filename, 'r') as f_2:
    pred_prec = f_2['predicted'][()]    

plt_2.rcParams["figure.figsize"] = [7.0, 3.5]
plt_2.rcParams["figure.autolayout"] = True
fig_2, ax_2 = plt_2.subplots()
ax_2.axis('off')

fig_2.set_tight_layout(False) 

init_frequency = 0
plt_2.title ("Precipitation")
axfreq_2 = plt_2.axes([0.2, 0.01, 0.7, 0.05])
freq_slider_2 = Slider(
    ax=axfreq_2,
    label='Prediction:',
    valmin=0,
    valmax=40,
    valstep=1,
    valinit=init_frequency,
)

image_2 = pred_prec[0][0][0]
color_map ='PuBuGn'
inter = 'nearest'

img_2 = ax_2.imshow(image_2[::,::], cmap=color_map, interpolation = inter)     
def update(val):
   image_2 = pred_prec[0][val][0]
   ax_2.imshow(image[::,::], cmap=color_map, interpolation = inter)

freq_slider_2.on_changed(update) 
plt_2.show()


### 2.2 (update)

%matplotlib widget 
 
filename = '/scratch/fp0/mah900/globus_data/output/autoregressive_predictions_tp.h5'
with h5py.File(filename, 'r') as f2:
    pred_prec = f2['predicted'][()]    
    ground_t  = f2['ground_truth'][()]  
plt.rcParams["figure.figsize"] = [8, 8]
plt.rcParams["figure.autolayout"] = True
fig, ax = plt.subplots(2)
fig.canvas.toolbar_visible = False
fig.canvas.header_visible = False
fig.canvas.footer_visible = False
ax[0].set_title("Prediction")
ax[1].set_title("Ground Truth")
ax[0].axis('off')
ax[1].axis('off')
fig.set_tight_layout(False) 
init_frequency = 0
plt.suptitle ("Precipitation")
axfreq = plt.axes([0.95, 0.2, 0.03, 0.5])
freq_slider = Slider(
    ax=axfreq,
    label='Prediction:',
    valmin=0,
    valmax=30,
    valstep=1,
    valinit=init_frequency,
    orientation = 'vertical'
)
color_map ='Spectral_r'
inter = 'nearest'
img  = ax[0].imshow(pred_prec[0][0][0], cmap=color_map, interpolation = inter, vmin=0, vmax=0.005)   
img2 = ax[1].imshow(ground_t[0][0][0] , cmap=color_map, interpolation = inter, vmin=0, vmax=0.005)    
def update(val):
   #image = pred_prec[0][val][0]
   ax[0].imshow(pred_prec[0][val][0], cmap=color_map,  vmin=0, vmax=0.005)
   ax[1].imshow(ground_t[0][val][0] , cmap=color_map,  vmin=0, vmax=0.005)

freq_slider.on_changed(update) 
#plt.colorbar(img,ax=ax)
plt.show()


## Move data to wb00

 rsync -r --progress globus_data/*  /g/data/wb00/admin/staging/fourcastnet/

sending incremental file list
FCN_weights_v0/
FCN_weights_v0/backbone.ckpt
    896,430,597 100%  200.82MB/s    0:00:04 (xfr#1, to-chk=18/24)
FCN_weights_v0/precip.ckpt
    885,228,409 100%   99.26MB/s    0:00:08 (xfr#2, to-chk=17/24)
out_of_sample/
out_of_sample/2018.h5
127,329,755,000 100%  264.28MB/s    0:07:39 (xfr#3, to-chk=16/24)
output/
output/autoregressive_predictions_tp.h5
    340,075,488 100%  138.24MB/s    0:00:02 (xfr#4, to-chk=15/24)
output/autoregressive_predictions_z500.h5
        712,576 100%    1.43MB/s    0:00:00 (xfr#5, to-chk=14/24)
output/autoregressive_predictions_z500_vis.h5
  6,801,431,776 100%  259.87MB/s    0:00:24 (xfr#6, to-chk=13/24)
output/inference_out.log
        276,395 100%  246.50kB/s    0:00:01 (xfr#7, to-chk=12/24)
precip/
precip/out_of_sample/
precip/out_of_sample/2018.h5
  6,054,914,048 100%  270.86MB/s    0:00:21 (xfr#8, to-chk=10/24)
precip/out_of_sample/2019.h5
  6,054,914,048 100%  268.92MB/s    0:00:21 (xfr#9, to-chk=9/24)
stats_v0/
stats_v0/global_means.npy
            296 100%    0.59kB/s    0:00:00 (xfr#10, to-chk=8/24)
stats_v0/global_stds.npy
            296 100%    0.56kB/s    0:00:00 (xfr#11, to-chk=7/24)
stats_v0/land_sea_mask.npy
      8,306,048 100%    8.23MB/s    0:00:00 (xfr#12, to-chk=6/24)
stats_v0/latitude.npy
          3,012 100%    2.98kB/s    0:00:00 (xfr#13, to-chk=5/24)
stats_v0/longitude.npy
          5,888 100%    5.76kB/s    0:00:00 (xfr#14, to-chk=4/24)
stats_v0/time_means.npy
    174,424,448 100%   53.92MB/s    0:00:03 (xfr#15, to-chk=3/24)
stats_v0/time_means_daily.h5
127,329,755,000 100%  274.94MB/s    0:07:21 (xfr#16, to-chk=2/24)
stats_v0/precip/
stats_v0/precip/time_means.npy
      8,294,528 100%   15.07MB/s    0:00:00 (xfr#17, to-chk=0/24)


##### Check 

rsync -r --progress globus_data/*  /g/data/wb00/admin/staging/fourcastnet/      --checksum
    sending incremental file list

## Download orthography data    

#### start personal connect endpoint first
#### Then

   . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet

    fourcast_ep=945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    my_ep=85a9f386-5f53-11ed-b0b5-bfe7e7197080   
    globus ls ${fourcast_ep}:/~/data/FCN_ERA5_data_v0/static/
    globus ls ${my_ep}:/scratch/fp0/mah900/globus_data/    

    globus transfer ${fourcast_ep}:/~/data/FCN_ERA5_data_v0/static/ ${my_ep}:/scratch/fp0/mah900/globus_data/static/ --label orography -r -s checksum --verify-checksum --notify off 

    $ globus task show  47f994a2-6335-11ed-8422-8d2923bcb3ec
Label:                        orography
Task ID:                      47f994a2-6335-11ed-8422-8d2923bcb3ec
Is Paused:                    False
Type:                         TRANSFER
Directories:                  1
Files:                        1
Status:                       SUCCEEDED
Request Time:                 2022-11-13T09:26:45+00:00
Faults:                       0
Total Subtasks:               3
Subtasks Succeeded:           3
Subtasks Pending:             0
Subtasks Retrying:            0
Subtasks Failed:              0
Subtasks Canceled:            0
Subtasks Expired:             0
Subtasks with Skipped Errors: 0
Completion Time:              2022-11-13T09:26:56+00:00
Source Endpoint:              FourCastNet
Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
Destination Endpoint:         my_ep1
Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
Bytes Transferred:            8307968
Bytes Per Second:             739686

###############


### Missing optional dependency 'tables'. In pandas to_hdf
ImportError: Missing optional dependency 'tables'.  Use pip or conda to install tables.

https://stackoverflow.com/questions/58479748/missing-optional-dependency-tables-in-pandas-to-hdf

conda install pytables




## Visualization (ARE)
#### Orography and Land Sea Mask

import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rcParams

%matplotlib inline
rcParams['figure.figsize'] = (20,6)
filename = '/g/data/wb00/admin/staging/fourcastnet/static/orography.h5'
with h5py.File(filename, 'r') as f1:
    orog = f1['orog'][()] 
    
filename = '/g/data/wb00/admin/staging/fourcastnet/stats_v0/land_sea_mask.npy'
land_sea_mask = np.load(filename)
 
fig, ax = plt.subplots(1,2)
ax[0].set_title("Orography")
colormap = 'terrain' 
ax[0].imshow(orog, interpolation='nearest', cmap=colormap, vmin=-1.95)
ax[1].set_title("Land Sea Mask")
colormap = 'ocean'
ax[1].imshow(land_sea_mask, interpolation='nearest', cmap=colormap)
plt.show()


####  Daily means

%matplotlib inline
import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML
from matplotlib import rcParams
from matplotlib import rc

plt.rcParams["figure.figsize"] = [20.0, 6.0]
plt.rcParams["figure.autolayout"] = True
#filename = '/g/data/wb00/admin/staging/fourcastnet/stats_v0/time_means_daily.h5'
#with h5py.File(filename, 'r') as f1:
#    training_db = f1['time_means_daily']
#    image_arr = training_db [0,:,:,:]

filename = '/g/data/wb00/admin/staging/fourcastnet/stats_v0/time_means.npy'
time_means = np.load(filename)
#print (np.matrix(b[0][0]))  
#print (np.matrix(time_means[0].shape))     
    
    
#fig, ax = plt.subplots(1,2, sharey=True, sharex=True)
#fig2, ax2 = plt.subplots(1,2, sharey=True, sharex=True)
fig, ax = plt.subplots()
#ax[0].set_title("Time Means Daily")
#ax2[0].set_title("Time Means")
colormap = 'terrain'
def display_fun():
    ims = []
    #ims2 = []
     
    for i in range(0,21):
        #im = ax[0].imshow(image_arr[i], interpolation='nearest', cmap=colormap, animated=True) 
        #im2 = ax2[0].imshow(time_means[0][i], interpolation='nearest', cmap=colormap, animated=True) 
        im = ax.imshow(time_means[0][i], interpolation='nearest', cmap=colormap, animated=True) 
        if i == 0:
            #im = ax[0].imshow(image_arr[i], interpolation='nearest', cmap=colormap)    
            #im2 = ax2[0].imshow(time_means[0][i], interpolation='nearest', cmap=colormap, animated=True) 
            im = ax.imshow(time_means[0][i], interpolation='nearest', cmap=colormap) 
        ims.append([im])  
        #ims2.append([im2])  
    #return ims , ims2  
    return ims   
#ims, ims2 =  display_fun();    
ims =  display_fun(); 
ani = animation.ArtistAnimation(fig, ims, interval=500, blit=True, repeat_delay=1000)
#ani2 = animation.ArtistAnimation(fig, ims2, interval=500, blit=True, repeat_delay=1000)
plt.close()
rc('animation', html='jshtml')
ani
#ani2


#### Mean value files

import h5py
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
data_dir = '/g/data/wb00/admin/staging/fourcastnet'
print ( "global means:" )  
filename = data_dir + '/stats_v0/global_means.npy'
a = np.load(filename)
#print ( np.matrix(a[0]) )    
print ( np.matrix(a.shape) )  
print ( "time means:" )  
filename = data_dir + '/stats_v0/time_means.npy'
b = np.load(filename)
#print (np.matrix(b[0][0]))  
print (np.matrix(b[0].shape)) 
print ( "global stds:" )  
filename = data_dir + '/stats_v0/global_stds.npy'
c = np.load(filename)
#print ( np.matrix(c[0]) ) 
print ( np.matrix(c.shape) ) 

#### Latitute

#import h5py
#from matplotlib import pyplot as plt
#import numpy as np
#import pandas as pd
filename = '/g/data/wb00/admin/staging/fourcastnet/stats_v0/latitude.npy'
d = np.load(filename)
print ( d.shape )  


### Prediction, first interactive visualization, slider


%matplotlib widget 

filename = '/g/data/wb00/admin/staging/fourcastnet/output/autoregressive_predictions_z500_vis.h5'
with h5py.File(filename, 'r') as f1:
    pred = f1['predicted'][()]   
    ground_t = f1['ground_truth'][()]  

plt.rcParams["figure.figsize"] = [8,8]
plt.rcParams["figure.autolayout"] = True
fig, ax = plt.subplots(2)
fig.canvas.toolbar_visible = False
fig.canvas.header_visible = False
fig.canvas.footer_visible = False
ax[0].axis('off')
ax[1].axis('off')
fig.set_tight_layout(False) 
init_frequency = 0
plt.suptitle ("Inference") 
ax[0].set_title("Prediction")
ax[1].set_title("Ground Truth")
axfreq = plt.axes([0.92, 0.2, 0.03, 0.5])
freq_slider = Slider(
    ax=axfreq,
    label='Prediction:',
    valmin=0,
    valmax=30,
    valstep=1,
    valinit=init_frequency,
    orientation = 'vertical'
)
layer = 19
assert layer >= 0 and layer < 20, "layer should be between 0 and 19"
image = np.empty((30, 720, 1440))
ground_image = np.empty((30, 720, 1440))
for i in range (0, 30):
    image[i][:][:] =   pred[0][i][layer].copy()
    ground_image[i][:][:] = ground_t[0][i][layer].copy()
    
color_map ='Spectral_r'
inter = 'nearest'
mi = np.min (image[0])
mx = np.max (image[0])
img1 = ax[0].imshow(image[0] , cmap=color_map, interpolation = inter , vmin=mi, vmax=mx)  
img2 = ax[1].imshow(ground_image[0] , cmap=color_map, interpolation = inter, vmin=mi, vmax=mx )  
def update(val):
   ax[0].imshow(image[val] , cmap=color_map, interpolation = inter , vmin=mi, vmax=mx)
   ax[1].imshow(ground_image[val] , cmap=color_map, interpolation = inter , vmin=mi, vmax=mx) 
freq_slider.on_changed(update) 
#cbaxes = fig.add_axes([0.05, 0.2, 0.01, 0.5]) 
#plt.colorbar(img1,ax=ax, cax = cbaxes)
#plt.show()


###  Precipitation prediction, first visualization, slider.

%matplotlib widget 
 
filename = '/g/data/wb00/admin/staging/fourcastnet/output/autoregressive_predictions_tp.h5'
with h5py.File(filename, 'r') as f2:
    pred_prec = f2['predicted'][()]    
    ground_t  = f2['ground_truth'][()]  
plt.rcParams["figure.figsize"] = [8, 8]
plt.rcParams["figure.autolayout"] = True
fig, ax = plt.subplots(2)
fig.canvas.toolbar_visible = False
fig.canvas.header_visible = False
fig.canvas.footer_visible = False
ax[0].set_title("Prediction")
ax[1].set_title("Ground Truth")
ax[0].axis('off')
ax[1].axis('off')
fig.set_tight_layout(False) 
init_frequency = 0
plt.suptitle ("Precipitation")
axfreq = plt.axes([0.92, 0.2, 0.03, 0.5])
freq_slider = Slider(
    ax=axfreq,
    label='Prediction:',
    valmin=0,
    valmax=30,
    valstep=1,
    valinit=init_frequency,
    orientation = 'vertical'
)
color_map ='Spectral_r'
inter = 'nearest'
img  = ax[0].imshow(pred_prec[0][0][0], cmap=color_map, interpolation = inter, vmin=0, vmax=0.005)   
img2 = ax[1].imshow(ground_t[0][0][0] , cmap=color_map, interpolation = inter, vmin=0, vmax=0.005)    
def update(val):
   #image = pred_prec[0][val][0]
   ax[0].imshow(pred_prec[0][val][0], cmap=color_map,  vmin=0, vmax=0.005)
   ax[1].imshow(ground_t[0][val][0] , cmap=color_map,  vmin=0, vmax=0.005)

freq_slider.on_changed(update) 
#plt.colorbar(img,ax=ax)
plt.show()

===============================================


