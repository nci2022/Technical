### Installation

https://docs.nvidia.com/deeplearning/modulus/user_guide/getting_started/installation.html


    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

    conda create -p /scratch/fp0/mah900/env/fourcastnet python=3.8

    conda activate /scratch/fp0/mah900/env/fourcastnet

=====================================================================================  

### The best recommended way is to use latest version for both PyTorch and functorch.
https://pytorch.org/get-started/locally/

### PyTorch Build: Stable (1.13.0)
### Linux
### Conda
### Python
### CUDA 11.7
    conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

## Package Plan ##

  environment location: /scratch/fp0/mah900/env/fourcastnet

  added / updated specs:
    - pytorch
    - pytorch-cuda=11.7
    - torchaudio
    - torchvision


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    blas-1.0                   |              mkl           6 KB
    cuda-11.7.1                |                0           1 KB  nvidia
    cuda-cccl-11.7.91          |                0         1.2 MB  nvidia
    cuda-command-line-tools-11.7.1|                0           1 KB  nvidia
    cuda-compiler-11.7.1       |                0           1 KB  nvidia
    cuda-cudart-11.7.99        |                0         194 KB  nvidia
    cuda-cudart-dev-11.7.99    |                0         1.1 MB  nvidia
    cuda-cuobjdump-11.7.91     |                0         158 KB  nvidia
    cuda-cupti-11.7.101        |                0        22.9 MB  nvidia
    cuda-cuxxfilt-11.7.91      |                0         293 KB  nvidia
    cuda-demo-suite-11.8.86    |                0         5.0 MB  nvidia
    cuda-documentation-11.8.86 |                0          89 KB  nvidia
    cuda-driver-dev-11.7.99    |                0          16 KB  nvidia
    cuda-gdb-11.8.86           |                0         4.8 MB  nvidia
    cuda-libraries-11.7.1      |                0           1 KB  nvidia
    cuda-libraries-dev-11.7.1  |                0           2 KB  nvidia
    cuda-memcheck-11.8.86      |                0         168 KB  nvidia
    cuda-nsight-11.8.86        |                0       113.6 MB  nvidia
    cuda-nsight-compute-11.8.0 |                0           1 KB  nvidia
    cuda-nvcc-11.7.99          |                0        42.7 MB  nvidia
    cuda-nvdisasm-11.8.86      |                0        48.7 MB  nvidia
    cuda-nvml-dev-11.7.91      |                0          80 KB  nvidia
    cuda-nvprof-11.8.87        |                0         4.4 MB  nvidia
    cuda-nvprune-11.7.91       |                0          64 KB  nvidia
    cuda-nvrtc-11.7.99         |                0        17.3 MB  nvidia
    cuda-nvrtc-dev-11.7.99     |                0        16.9 MB  nvidia
    cuda-nvtx-11.7.91          |                0          57 KB  nvidia
    cuda-nvvp-11.8.87          |                0       114.4 MB  nvidia
    cuda-runtime-11.7.1        |                0           1 KB  nvidia
    cuda-sanitizer-api-11.8.86 |                0        16.6 MB  nvidia
    cuda-toolkit-11.7.1        |                0           1 KB  nvidia
    cuda-tools-11.7.1          |                0           1 KB  nvidia
    cuda-visual-tools-11.7.1   |                0           1 KB  nvidia
    ffmpeg-4.3                 |       hf484d3e_0         9.9 MB  pytorch
    freetype-2.12.1            |       h4a9f257_0         626 KB
    gds-tools-1.4.0.31         |                0           2 KB  nvidia
    giflib-5.2.1               |       h7b6447c_0          78 KB
    gmp-6.2.1                  |       h295c915_3         544 KB
    gnutls-3.6.15              |       he1e5248_0         1.0 MB
    intel-openmp-2021.4.0      |    h06a4308_3561         4.2 MB
    jpeg-9e                    |       h7f8727e_0         240 KB
    lame-3.100                 |       h7b6447c_0         323 KB
    lcms2-2.12                 |       h3be6417_0         312 KB
    lerc-3.0                   |       h295c915_0         196 KB
    libcublas-11.11.3.6        |                0       364.0 MB  nvidia
    libcublas-dev-11.11.3.6    |                0       394.1 MB  nvidia
    libcufft-10.9.0.58         |                0       142.8 MB  nvidia
    libcufft-dev-10.9.0.58     |                0       275.8 MB  nvidia
    libcufile-1.4.0.31         |                0         548 KB  nvidia
    libcufile-dev-1.4.0.31     |                0         1.6 MB  nvidia
    libcurand-10.3.0.86        |                0        53.2 MB  nvidia
    libcurand-dev-10.3.0.86    |                0        53.7 MB  nvidia
    libcusolver-11.4.1.48      |                0        96.5 MB  nvidia
    libcusolver-dev-11.4.1.48  |                0        66.3 MB  nvidia
    libcusparse-11.7.5.86      |                0       176.3 MB  nvidia
    libcusparse-dev-11.7.5.86  |                0       359.7 MB  nvidia
    libdeflate-1.8             |       h7f8727e_5          51 KB
    libidn2-2.3.2              |       h7f8727e_0          81 KB
    libnpp-11.8.0.86           |                0       147.8 MB  nvidia
    libnpp-dev-11.8.0.86       |                0       144.5 MB  nvidia
    libnvjpeg-11.9.0.86        |                0         2.4 MB  nvidia
    libnvjpeg-dev-11.9.0.86    |                0         2.1 MB  nvidia
    libpng-1.6.37              |       hbc83047_0         278 KB
    libtasn1-4.16.0            |       h27cfd23_0          58 KB
    libtiff-4.4.0              |       hecacb30_1         516 KB
    libunistring-0.9.10        |       h27cfd23_0         536 KB
    libwebp-1.2.4              |       h11a3e52_0          79 KB
    libwebp-base-1.2.4         |       h5eee18b_0         347 KB
    mkl-2021.4.0               |     h06a4308_640       142.6 MB
    mkl-service-2.4.0          |   py38h7f8727e_0          59 KB
    mkl_fft-1.3.1              |   py38hd3c417c_0         180 KB
    mkl_random-1.2.2           |   py38h51133e4_0         308 KB
    nettle-3.7.3               |       hbbd107a_1         809 KB
    nsight-compute-2022.3.0.22 |                0       610.0 MB  nvidia
    numpy-1.23.3               |   py38h14f4228_1          10 KB
    numpy-base-1.23.3          |   py38h31eccc5_1         6.7 MB
    openh264-2.1.1             |       h4ff587b_0         711 KB
    pillow-9.2.0               |   py38hace64e9_1         666 KB
    pytorch-1.13.0             |py3.8_cuda11.7_cudnn8.5.0_0        1.15 GB  pytorch
    pytorch-cuda-11.7          |       h67b0de4_0           7 KB  pytorch
    pytorch-mutex-1.0          |             cuda           3 KB  pytorch
    torchaudio-0.13.0          |       py38_cu117         6.5 MB  pytorch
    torchvision-0.14.0         |       py38_cu117        29.5 MB  pytorch
    zstd-1.5.2                 |       ha4553b6_0         488 KB
    ------------------------------------------------------------
                                           Total:        4.57 GB

The following NEW packages will be INSTALLED:

  blas               pkgs/main/linux-64::blas-1.0-mkl
  brotlipy           pkgs/main/linux-64::brotlipy-0.7.0-py38h27cfd23_1003
  bzip2              pkgs/main/linux-64::bzip2-1.0.8-h7b6447c_0
  cffi               pkgs/main/linux-64::cffi-1.15.1-py38h74dc2b5_0
  charset-normalizer pkgs/main/noarch::charset-normalizer-2.0.4-pyhd3eb1b0_0
  cryptography       pkgs/main/linux-64::cryptography-38.0.1-py38h9ce1e76_0
  cuda               nvidia/linux-64::cuda-11.7.1-0
  cuda-cccl          nvidia/linux-64::cuda-cccl-11.7.91-0
  cuda-command-line~ nvidia/linux-64::cuda-command-line-tools-11.7.1-0
  cuda-compiler      nvidia/linux-64::cuda-compiler-11.7.1-0
  cuda-cudart        nvidia/linux-64::cuda-cudart-11.7.99-0
  cuda-cudart-dev    nvidia/linux-64::cuda-cudart-dev-11.7.99-0
  cuda-cuobjdump     nvidia/linux-64::cuda-cuobjdump-11.7.91-0
  cuda-cupti         nvidia/linux-64::cuda-cupti-11.7.101-0
  cuda-cuxxfilt      nvidia/linux-64::cuda-cuxxfilt-11.7.91-0
  cuda-demo-suite    nvidia/linux-64::cuda-demo-suite-11.8.86-0
  cuda-documentation nvidia/linux-64::cuda-documentation-11.8.86-0
  cuda-driver-dev    nvidia/linux-64::cuda-driver-dev-11.7.99-0
  cuda-gdb           nvidia/linux-64::cuda-gdb-11.8.86-0
  cuda-libraries     nvidia/linux-64::cuda-libraries-11.7.1-0
  cuda-libraries-dev nvidia/linux-64::cuda-libraries-dev-11.7.1-0
  cuda-memcheck      nvidia/linux-64::cuda-memcheck-11.8.86-0
  cuda-nsight        nvidia/linux-64::cuda-nsight-11.8.86-0
  cuda-nsight-compu~ nvidia/linux-64::cuda-nsight-compute-11.8.0-0
  cuda-nvcc          nvidia/linux-64::cuda-nvcc-11.7.99-0
  cuda-nvdisasm      nvidia/linux-64::cuda-nvdisasm-11.8.86-0
  cuda-nvml-dev      nvidia/linux-64::cuda-nvml-dev-11.7.91-0
  cuda-nvprof        nvidia/linux-64::cuda-nvprof-11.8.87-0
  cuda-nvprune       nvidia/linux-64::cuda-nvprune-11.7.91-0
  cuda-nvrtc         nvidia/linux-64::cuda-nvrtc-11.7.99-0
  cuda-nvrtc-dev     nvidia/linux-64::cuda-nvrtc-dev-11.7.99-0
  cuda-nvtx          nvidia/linux-64::cuda-nvtx-11.7.91-0
  cuda-nvvp          nvidia/linux-64::cuda-nvvp-11.8.87-0
  cuda-runtime       nvidia/linux-64::cuda-runtime-11.7.1-0
  cuda-sanitizer-api nvidia/linux-64::cuda-sanitizer-api-11.8.86-0
  cuda-toolkit       nvidia/linux-64::cuda-toolkit-11.7.1-0
  cuda-tools         nvidia/linux-64::cuda-tools-11.7.1-0
  cuda-visual-tools  nvidia/linux-64::cuda-visual-tools-11.7.1-0
  ffmpeg             pytorch/linux-64::ffmpeg-4.3-hf484d3e_0
  freetype           pkgs/main/linux-64::freetype-2.12.1-h4a9f257_0
  gds-tools          nvidia/linux-64::gds-tools-1.4.0.31-0
  giflib             pkgs/main/linux-64::giflib-5.2.1-h7b6447c_0
  gmp                pkgs/main/linux-64::gmp-6.2.1-h295c915_3
  gnutls             pkgs/main/linux-64::gnutls-3.6.15-he1e5248_0
  idna               pkgs/main/linux-64::idna-3.4-py38h06a4308_0
  intel-openmp       pkgs/main/linux-64::intel-openmp-2021.4.0-h06a4308_3561
  jpeg               pkgs/main/linux-64::jpeg-9e-h7f8727e_0
  lame               pkgs/main/linux-64::lame-3.100-h7b6447c_0
  lcms2              pkgs/main/linux-64::lcms2-2.12-h3be6417_0
  lerc               pkgs/main/linux-64::lerc-3.0-h295c915_0
  libcublas          nvidia/linux-64::libcublas-11.11.3.6-0
  libcublas-dev      nvidia/linux-64::libcublas-dev-11.11.3.6-0
  libcufft           nvidia/linux-64::libcufft-10.9.0.58-0
  libcufft-dev       nvidia/linux-64::libcufft-dev-10.9.0.58-0
  libcufile          nvidia/linux-64::libcufile-1.4.0.31-0
  libcufile-dev      nvidia/linux-64::libcufile-dev-1.4.0.31-0
  libcurand          nvidia/linux-64::libcurand-10.3.0.86-0
  libcurand-dev      nvidia/linux-64::libcurand-dev-10.3.0.86-0
  libcusolver        nvidia/linux-64::libcusolver-11.4.1.48-0
  libcusolver-dev    nvidia/linux-64::libcusolver-dev-11.4.1.48-0
  libcusparse        nvidia/linux-64::libcusparse-11.7.5.86-0
  libcusparse-dev    nvidia/linux-64::libcusparse-dev-11.7.5.86-0
  libdeflate         pkgs/main/linux-64::libdeflate-1.8-h7f8727e_5
  libiconv           pkgs/main/linux-64::libiconv-1.16-h7f8727e_2
  libidn2            pkgs/main/linux-64::libidn2-2.3.2-h7f8727e_0
  libnpp             nvidia/linux-64::libnpp-11.8.0.86-0
  libnpp-dev         nvidia/linux-64::libnpp-dev-11.8.0.86-0
  libnvjpeg          nvidia/linux-64::libnvjpeg-11.9.0.86-0
  libnvjpeg-dev      nvidia/linux-64::libnvjpeg-dev-11.9.0.86-0
  libpng             pkgs/main/linux-64::libpng-1.6.37-hbc83047_0
  libtasn1           pkgs/main/linux-64::libtasn1-4.16.0-h27cfd23_0
  libtiff            pkgs/main/linux-64::libtiff-4.4.0-hecacb30_1
  libunistring       pkgs/main/linux-64::libunistring-0.9.10-h27cfd23_0
  libwebp            pkgs/main/linux-64::libwebp-1.2.4-h11a3e52_0
  libwebp-base       pkgs/main/linux-64::libwebp-base-1.2.4-h5eee18b_0
  lz4-c              pkgs/main/linux-64::lz4-c-1.9.3-h295c915_1
  mkl                pkgs/main/linux-64::mkl-2021.4.0-h06a4308_640
  mkl-service        pkgs/main/linux-64::mkl-service-2.4.0-py38h7f8727e_0
  mkl_fft            pkgs/main/linux-64::mkl_fft-1.3.1-py38hd3c417c_0
  mkl_random         pkgs/main/linux-64::mkl_random-1.2.2-py38h51133e4_0
  nettle             pkgs/main/linux-64::nettle-3.7.3-hbbd107a_1
  nsight-compute     nvidia/linux-64::nsight-compute-2022.3.0.22-0
  numpy              pkgs/main/linux-64::numpy-1.23.3-py38h14f4228_1
  numpy-base         pkgs/main/linux-64::numpy-base-1.23.3-py38h31eccc5_1
  openh264           pkgs/main/linux-64::openh264-2.1.1-h4ff587b_0
  pillow             pkgs/main/linux-64::pillow-9.2.0-py38hace64e9_1
  pycparser          pkgs/main/noarch::pycparser-2.21-pyhd3eb1b0_0
  pyopenssl          pkgs/main/noarch::pyopenssl-22.0.0-pyhd3eb1b0_0
  pysocks            pkgs/main/linux-64::pysocks-1.7.1-py38h06a4308_0
  pytorch            pytorch/linux-64::pytorch-1.13.0-py3.8_cuda11.7_cudnn8.5.0_0
  pytorch-cuda       pytorch/noarch::pytorch-cuda-11.7-h67b0de4_0
  pytorch-mutex      pytorch/noarch::pytorch-mutex-1.0-cuda
  requests           pkgs/main/linux-64::requests-2.28.1-py38h06a4308_0
  six                pkgs/main/noarch::six-1.16.0-pyhd3eb1b0_1
  torchaudio         pytorch/linux-64::torchaudio-0.13.0-py38_cu117
  torchvision        pytorch/linux-64::torchvision-0.14.0-py38_cu117
  typing_extensions  pkgs/main/linux-64::typing_extensions-4.3.0-py38h06a4308_0
  urllib3            pkgs/main/linux-64::urllib3-1.26.12-py38h06a4308_0
  zstd               pkgs/main/linux-64::zstd-1.5.2-ha4553b6_0


Proceed ([y]/n)? y

=====================================================================================

### Modulus Bare Metal Install

pip3 install matplotlib transforms3d future typing numpy quadpy\
             numpy-stl==2.16.3 h5py sympy==1.5.1 termcolor psutil\
             symengine==0.6.1 numba Cython chaospy torch_optimizer\
             vtk chaospy termcolor omegaconf hydra-core==1.1.1 einops\
             timm tensorboard pandas orthopy ndim functorch pint


====================================================================================

All examples can be found in the Modulus GitLab repository. To get access to the GitLab repo, visit the NVIDIA Modulus Downloads page and sign up for the Modulus GitLab Repository Access .

https://developer.nvidia.com/modulus-downloads

#### maruf.ahmed
### Access granted 
    https://gitlab.com/nvidia/modulus

docs, examples, Modulus, chkeckpoints. 

https://gitlab.com/nvidia/modulus/modulus.git

### Gitlab
<!-- 
glpat-5BTm71xDFSTvbRgynjUx
glpat-5BTm71xDFSTvbRgynjUx
-->

### https://stackoverflow.com/questions/25409700/using-gitlab-token-to-clone-without-authentication
git clone https://oauth2:ACCESS_TOKEN@somegitlab.com/vendor/package.git

====================================================================================

### Once all dependencies are installed, the Modulus source code can be downloaded 

    cd /scratch/fp0/mah900/
<!--
    git clone https://oauth2:glpat-5BTm71xDFSTvbRgynjUx@gitlab.com/nvidia/modulus/modulus.git
-->

    Cloning into 'modulus'...
    remote: Enumerating objects: 9356, done.
    remote: Total 9356 (delta 0), reused 0 (delta 0), pack-reused 9356
    Receiving objects: 100% (9356/9356), 7.07 MiB | 13.07 MiB/s, done.
    Resolving deltas: 100% (6524/6524), done.

=================================================================================

    cd /scratch/fp0/mah900/modulus/
    python setup.py install


### Using the Modulus examples
    cd /scratch/fp0/mah900/
<!--
    git clone https://oauth2:glpat-5BTm71xDFSTvbRgynjUx@gitlab.com/nvidia/modulus/examples.git  modulus_examples
 -->

    Cloning into 'modulus_examples'...
    remote: Enumerating objects: 4715, done.
    remote: Counting objects: 100% (534/534), done.
    remote: Compressing objects: 100% (255/255), done.
    remote: Total 4715 (delta 281), reused 446 (delta 258), pack-reused 4181
    Receiving objects: 100% (4715/4715), 3.61 MiB | 3.30 MiB/s, done.
    Resolving deltas: 100% (2700/2700), done.

### To verify the installation has been done correctly, run these commands:

    cd /scratch/fp0/mah900/modulus_examples/helmholtz/
    python helmholtz.py

### ValueError: could not convert string 'oid sha256:aeac30245e5ac347f39c4c076101decc448c674dd1531fe200a1c5e598c9557c' to float64 at row 0, column 1.

https://forums.developer.nvidia.com/t/unable-to-run-helmholtz-py-after-installing-22-03-1/221919/6?u=mahm1846

Looks like you don’t have Git LFS installed. We use Git LFS in our examples repo to store the data files, without LFS it will just be a pointer file. With Git LFS installed, try re-cloning the examples repo. That CSV file for LDC should now have proper data in it and the example should run.

More information about Git LFS can be found here (https://git-lfs.github.com/)

### Installing Git Large File Storage
https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage?platform=linux

### pip install git-lfs
### did not work

    pip install --upgrade pip

### conda-forge / packages / git-lfs 3.2.0
https://anaconda.org/conda-forge/git-lfs/
    conda install -c conda-forge git-lfs

    git lfs install

    rm -rf /scratch/fp0/mah900/modulus_examples 
    cd /scratch/fp0/mah900/
<!--
    git clone https://oauth2:glpat-5BTm71xDFSTvbRgynjUx@gitlab.com/nvidia/modulus/examples.git  modulus_examples
 -->

    Cloning into 'modulus_examples'...
    remote: Enumerating objects: 4715, done.
    remote: Counting objects: 100% (534/534), done.
    remote: Compressing objects: 100% (255/255), done.
    remote: Total 4715 (delta 281), reused 446 (delta 258), pack-reused 4181
    Receiving objects: 100% (4715/4715), 3.61 MiB | 6.34 MiB/s, done.
    Resolving deltas: 100% (2700/2700), done.
    Filtering content: 100% (79/79), 2.84 GiB | 85.00 MiB/s, done.


### Test 

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    cd /scratch/fp0/mah900/modulus_examples/helmholtz/
    python helmholtz.py

### RuntimeError: CUDA error: no CUDA-capable device is detected
CUDA kernel errors might be asynchronously reported at some other API call,so the stacktrace below might be incorrect.
For debugging consider passing CUDA_LAUNCH_BLOCKING=1.    

### Test with GPU

    qsub -I /home/900/mah900/conda/g1_h2.pbs
    qsub: waiting for job 63005162.gadi-pbs to start
    qsub: job 63005162.gadi-pbs ready

### On GPU node
    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    cd /scratch/fp0/mah900/modulus_examples/helmholtz/
    python helmholtz.py

### output

    [16:45:41] - JIT using the NVFuser TorchScript backend
    [16:45:41] - JitManager: {'_enabled': True, '_arch_mode': <JitArchMode.ONLY_ACTIVATION: 1>, '_use_nvfuser': True, '_autograd_nodes': False}
    [16:45:41] - GraphManager: {'_func_arch': False, '_debug': False, '_func_arch_allow_partial_hessian': True}
    [16:45:50] - Installed PyTorch version 1.13.0 is not TorchScript supported in Modulus. Version 1.13.0a0+d321be6 is officially supported.
    [16:45:50] - attempting to restore from: outputs/helmholtz
    [16:45:50] - optimizer checkpoint not found
    [16:45:50] - model wave_network.0.pth not found
    [16:45:58] - [step:          0] record constraint batch time:  1.065e+00s
    [16:45:59] - [step:          0] record validators time:  8.926e-01s
    [16:45:59] - [step:          0] saved checkpoint to outputs/helmholtz
    [16:45:59] - [step:          0] loss:  9.904e+03
    [16:46:03] - Attempting cuda graph building, this may take a bit...
    [16:46:06] - [step:        100] loss:  7.395e+03, time/iteration:  7.056e+01 ms

    ...

    [16:58:15] - [step:      19800] loss:  1.197e-02, time/iteration:  3.568e+01 ms
    [16:58:18] - [step:      19900] loss:  1.205e-02, time/iteration:  3.568e+01 ms
    [16:58:22] - [step:      20000] record constraint batch time:  3.654e-02s
    [16:58:23] - [step:      20000] record validators time:  5.325e-01s
    [16:58:23] - [step:      20000] saved checkpoint to outputs/helmholtz
    [16:58:23] - [step:      20000] loss:  1.125e-02, time/iteration:  4.792e+01 ms
    [16:58:23] - [step:      20000] reached maximum training steps, finished training!

=================================================================================================


### 2023.01.11
### Install in short (from above)

 . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

conda activate /scratch/fp0/mah900/env/fourcastnet

#######################################################################################
### 2023.01.19
### Re-install the original env (from above)

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    rm -r /scratch/fp0/mah900/env/fourcastnet

    module load openmpi/4.1.4 
    conda create -p /scratch/fp0/mah900/env/fourcastnet python=3.8
    conda activate /scratch/fp0/mah900/env/fourcastnet
    conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

    pip3 install matplotlib transforms3d future typing numpy quadpy\
             numpy-stl==2.16.3 h5py sympy==1.5.1 termcolor psutil\
             symengine==0.6.1 numba Cython chaospy torch_optimizer\
             vtk chaospy termcolor omegaconf hydra-core==1.1.1 einops\
             timm tensorboard pandas orthopy ndim functorch pint

    conda install -c conda-forge git-lfs
    git lfs install

    #rm -rf /scratch/fp0/mah900/modulus_examples 
    #cd /scratch/fp0/mah900/   
<!-- 
    git clone https://oauth2:glpat-5BTm71xDFSTvbRgynjUx@gitlab.com/nvidia/modulus/examples.git  modulus_examples
 -->

    cd /scratch/fp0/mah900/modulus/
    python setup.py install     

### Test with GPU (Needs GPU)
    # Interactive mode
    qsub -I /home/900/mah900/conda/g1_h2.pbs

#### On the GPU node
    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    cd /scratch/fp0/mah900/modulus_examples/helmholtz/
    python helmholtz.py

