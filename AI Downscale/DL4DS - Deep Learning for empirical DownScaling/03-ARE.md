### DL4DS - ARE

https://are.nci.org.au/

4
gpuvolta
1gpu
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

cuda/11.2.2 cudnn/8.1.1-cuda11
#/scratch/fp0/mah900/Miniconda/
/scratch/fp0/mah900/Miniconda2023/  #2023.01.31
/scratch/fp0/mah900/env/empirical_downscaling

####################
File: 
scratch/fp0/mah900/empirical_downscaling/Notebook-1.ipynb

#######################
import os
os.chdir('/scratch/fp0/mah900/empirical_downscaling/')
unscaled_y_pred = np.load('unscaled_y_pred.npy')
coarsened_array = np.load('coarsened_array.npy')
unscaled_y_test = np.load('unscaled_y_test.npy')