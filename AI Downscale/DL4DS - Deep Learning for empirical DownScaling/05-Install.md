

### New Install 2023.01.30-31

#### 1
module purge
rm -r /scratch/fp0/mah900/env/empirical_downscaling
#### 2
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
#### 3
#mkdir /scratch/fp0/mah900/empirical_downscaling
cd /scratch/fp0/mah900/empirical_downscaling

#### 4 
module load cuda/11.2.2 cudnn/8.1.1-cuda11

#### 4
conda create -p /scratch/fp0/mah900/env/empirical_downscaling python=3.8.* cartopy jupyterlab scipy matplotlib basemap -c conda-forge

conda activate /scratch/fp0/mah900/env/empirical_downscaling 

pip uninstall tensorflow
pip install dl4ds climetlab climetlab_maelstrom_downscaling scikit-learn 
#pip install -U tensorflow==2.10

#### Test

import numpy as np
import xarray as xr
import ecubevis as ecv
import dl4ds as dds
import scipy as sp
import netCDF4 as nc
import climetlab as cml
import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras import models

### Warning
Tensorflow is not working with CUDA, Load CUDA drivers before install, next time.

### Error 
libdevice.10.bc not found

### Nex Try -2023.01.31 (DID NOT work) 
#### Version mismatch, see below for a working combination

module purge
rm -r /scratch/fp0/mah900/env/empirical_downscaling
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
#mkdir /scratch/fp0/mah900/empirical_downscaling
cd /scratch/fp0/mah900/empirical_downscaling

conda create -p /scratch/fp0/mah900/env/empirical_downscaling python=3.9 cudatoolkit=11.2 cudnn=8.1.0 cartopy jupyterlab scipy matplotlib basemap -c conda-forge

conda activate /scratch/fp0/mah900/env/empirical_downscaling 

export XLA_FLAGS=--xla_gpu_cuda_data_dir=$CONDA_PREFIX

pip install tensorflow==2.9 dl4ds climetlab climetlab_maelstrom_downscaling scikit-learn 

### Next Try -2023.01.31 (Working)

module purge
rm -r /scratch/fp0/mah900/env/empirical_downscaling
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/empirical_downscaling python=3.8 cartopy jupyterlab scipy matplotlib basemap -c conda-forge
conda activate /scratch/fp0/mah900/env/empirical_downscaling 

cd /scratch/fp0/mah900/empirical_downscaling
module load  cuda/11.2.2
module load cudnn/8.1.1-cuda11
python -m pip uninstall tensorflow
python -m pip install numpy==1.20.* tensorflow==2.10 dl4ds climetlab climetlab_maelstrom_downscaling scikit-learn 

#### Warning
This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN)to use the following CPU instructions in performance-critical operations:  AVX AVX2

This is not an error, it is just telling you that it can and will take advantage of your CPU to get that extra speed out.
Note: AVX stands for Advanced Vector Extensions.
https://stackoverflow.com/questions/65298241/what-does-this-tensorflow-message-mean-any-side-effect-was-the-installation-su

#### Error size mismatch, while testing
ValueError: Input 0 of layer "resnet_spc" is incompatible with the layer: expected shape=(None, 28, 28, 1), found shape=(8, 28, 32, 2)
##### Try
crop Xarray data
cropped_ds = ds.sel(lat=slice(min_lat,max_lat), lon=slice(min_lon,max_lon))
https://gis.stackexchange.com/a/429268