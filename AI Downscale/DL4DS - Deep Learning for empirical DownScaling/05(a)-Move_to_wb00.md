### Move files to wb00 (2023.02.01)

 cp /scratch/fp0/mah900/empirical_downscaling/Notebook-1.ipynb /g/data/wb00/admin/staging/DL4DS

 ### ARE

In the Dashboard for JupyterLab launch, fill up the followings.
Walltime (hours):1
Queue: gpuvolta
Compute Size: 1gpu
Project: your own project (or fp0 to test).
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

In the Advanced options section: 
Modules: cuda/11.2.2 cudnn/8.1.1-cuda11
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda2023/
Conda environment: /scratch/fp0/mah900/env/empirical_downscaling

### CP data files
cd /scratch/fp0/mah900/empirical_downscaling/
cp coarsened_array.npy unscaled_y_pred.npy unscaled_y_test.npy /g/data/wb00/admin/staging/DL4DS

chmod -R 755 .
cp DL4DS.png /g/data/wb00/admin/staging/DL4DS
### Change in Notebook
 
g/data/wb00/admin/staging/DL4DS/Notebook-1.ipynb

import os
os.chdir("/g/data/wb00/admin/staging/DL4DS")