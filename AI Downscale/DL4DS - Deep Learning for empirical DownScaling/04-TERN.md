### TERN dataset down scaling

### Download data
cd /g/data/wb00/admin/staging/TERN

wget https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/MaximumTemperature/tmaxscr.monthly.ccam10_ACCESS1-0Q_rcp45.nc

### Install basemap

. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/empirical_downscaling 

#conda install basemap
#Has conflict

#Try conda forge
https://anaconda.org/conda-forge/basemap
conda install -c conda-forge basemap (working)

### After reinstall (2023.01.31)
scratch/fp0/mah900/empirical_downscaling/TERN-01.ipynb


### Slice NetCDF data

### Error
Do not open netcdf file with (context manager)
https://github.com/pydata/xarray/issues/1001#issuecomment-246497484


### Slice data variable (netCDF)
using date2index
https://stackoverflow.com/a/51444903

### Error 
Exact date cannot be found, use select='nearest' option
https://gis.stackexchange.com/questions/142422/how-i-can-extract-a-subset-of-years-from-a-netcdf-file-e-g-to-use-in-cell-stat

Documentation
https://unidata.github.io/netcdf4-python/#date2index


### Use Test, Val, Train dataset
#### Copied file
scratch/fp0/mah900/empirical_downscaling/TERN-02.ipynb

#### Slice variable (Xarray)
https://stackoverflow.com/a/51976270

### Created notebooks

#### Monthly Maximum Temperature
scratch/fp0/mah900/empirical_downscaling/TERN-02.ipynb

#### Monthly Solar Radiation
scratch/fp0/mah900/empirical_downscaling/TERN-03.ipynb

#### Wind Speed
scratch/fp0/mah900/empirical_downscaling/TERN-04.ipynb

### Error : TF out of memeory 
##### Try 1: (Not working)
	    gpus = tf.config.experimental.list_phyiscal_devices("GPU")
	    for gpu in gpus:
		    tf.config.experimental.set_memory_growth(gpu, True)
https://discuss.ray.io/t/tensorflow-allocates-all-available-memory-on-the-gpu-in-the-first-trial-leading-to-no-space-left-for-running-additional-trials-in-parallel/7585/2

#### Try2: (Working)
module load cuda/11.2.2 cudnn/8.1.1-cuda11
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/empirical_downscaling 
pip install numba 
 
device = cuda.get_current_device()
device.reset()
https://stackoverflow.com/a/60354785

### Warning 
This can cause out-of-memory errors in some cases. You must explicitly call `InteractiveSession.close()` to release resources held by the other session(s).

