### DL4DS Install

### DL4DS_tutorial notebook 
https://github.com/carlos-gg/dl4ds/blob/master/notebooks/DL4DS_tutorial.ipynb

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

    mkdir /scratch/fp0/mah900/empirical_downscaling
    cd /scratch/fp0/mah900/empirical_downscaling

    conda create -p /scratch/fp0/mah900/env/empirical_downscaling python=3.8.*
    conda activate /scratch/fp0/mah900/env/empirical_downscaling 

### Step 1
    pip install  dl4ds

### Error #1
    File "setup.py", line 390, in _populate_hdf5_info
      raise ValueError('did not find HDF5 headers')
  ValueError: did not find HDF5 headers
#### solution:
Activate conda first

### Error #2
  ERROR: Failed building wheel for cartopy
Successfully built sklearn
Failed to build cartopy
ERROR: Could not build wheels for cartopy, which is required to install pyproject.toml-based projects

#### solution:
    conda install -c conda-forge cartopy

From page, Conda pre-built binaries
https://scitools.org.uk/cartopy/docs/latest/installing.html


### Step 2
    pip install  climetlab
    pip install  climetlab_maelstrom_downscaling


### Error 3
Jupyter not starting
#### Solution 
Install Jupyter.
https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html
    conda install -c conda-forge jupyterlab


### Error 4
ModuleNotFoundError: No module named 'sklearn'
#### Solution
https://stackoverflow.com/questions/46113732/modulenotfounderror-no-module-named-sklearn
Even if conda is installed
    pip install -U scikit-learn scipy matplotlib

#### Error 5
ImportError: cannot import name '_rename_parameter' from 'scipy._lib._util' (/scratch/fp0/mah900/env/empirical_downscaling/lib/python3.8/site-packages/scipy/_lib/_util.py)
#### Solution
https://anaconda.org/anaconda/scipy
conda install -c anaconda scipy

(Updated version was installed)



### Step 3, ARE
go to : https://are-auth.nci.org.au/

2
gpuvolta
1gpu
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

#### Advanced
/scratch/fp0/mah900/Miniconda/
/scratch/fp0/mah900/env/empirical_downscaling/

#### Launch

### Error 6
    cmlds_train = cml.load_dataset("maelstrom-downscaling", dataset="training")
    cmlds_val = cml.load_dataset("maelstrom-downscaling", dataset="validation")
    cmlds_test = cml.load_dataset("maelstrom-downscaling", dataset="testing")

Failed to establish a new connection: [Errno 101] Network is unreachable'))], attemps 1 of 500 


#### Warning 
Bad key "text.kerning_factor" on line 4 in
#### Solution
https://stackoverflow.com/questions/61171307/jupyter-notebook-shows-error-message-for-matplotlib-bad-key-text-kerning-factor

    conda upgrade matplotlib
or
    pip install --upgrade matplotlib

#### Solution
Try from console 
    $ python
    # Downloads alright
    # Need to chage cache location

#### Frist, Need to set cache location
https://climetlab.readthedocs.io/en/latest/guide/caching.html   

$ climetlab settings cache-directory
/scratch/z00/mah900/tmp/climetlab-mah900

#### Change 
    mkdir -p /g/data/wb00/admin/staging/climetlab-mah900
    climetlab settings cache-directory /g/data/wb00/admin/staging/climetlab-mah900

#### Then, download from console
    python
    import climetlab as cml
    cache_path = cml.settings.get("cache-directory")
    print(cache_path)
    
    cmlds_train = cml.load_dataset("maelstrom-downscaling", dataset="training")
    cmlds_val = cml.load_dataset("maelstrom-downscaling", dataset="validation")
    cmlds_test = cml.load_dataset("maelstrom-downscaling", dataset="testing")

#### Then, Test access from notebook. 
    cache_path = cml.settings.get("cache-directory")
    print(cache_path)
    ...
    (Working)

#### Error
Running
    t2m_hr_train = cmlds_train.to_xarray().t2m_tar
    ... 
    z_lr_val = cmlds_val.to_xarray().z_in



### 2022.12.13

#### Error
Broadcast error on "y_z_train.values[0]" after adding channel dimension
solve after running the cells several times. 

##### Error
RuntimeError: NetCDF: Not a valid ID
Solution:
problem with jupyter and netCDF, trying script



    
    













