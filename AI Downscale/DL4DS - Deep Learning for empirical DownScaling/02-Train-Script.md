### Script
### Path: /scratch/fp0/mah900/empirical_downscaling/script-1.py
### Train

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 

import numpy as np
import xarray as xr
import ecubevis as ecv
import dl4ds as dds
import scipy as sp
import netCDF4 as nc
import climetlab as cml

import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras import models

#########
print ("ckp 1")
cmlds_train = cml.load_dataset("maelstrom-downscaling", dataset="training")
cmlds_val = cml.load_dataset("maelstrom-downscaling", dataset="validation")
cmlds_test = cml.load_dataset("maelstrom-downscaling", dataset="testing")
#########
t2m_hr_train = cmlds_train.to_xarray().t2m_tar
t2m_hr_test = cmlds_test.to_xarray().t2m_tar
t2m_hr_val = cmlds_val.to_xarray().t2m_tar

t2m_lr_train = cmlds_train.to_xarray().t2m_in
t2m_lr_test = cmlds_test.to_xarray().t2m_in
t2m_lr_val = cmlds_val.to_xarray().t2m_in

z_hr_train = cmlds_train.to_xarray().z_tar
z_hr_test = cmlds_test.to_xarray().z_tar
z_hr_val = cmlds_val.to_xarray().z_tar

z_lr_train = cmlds_train.to_xarray().z_in
z_lr_test = cmlds_test.to_xarray().z_in
z_lr_val = cmlds_val.to_xarray().z_in

########
t2m_scaler_train = dds.StandardScaler(axis=None)
t2m_scaler_train.fit(t2m_hr_train)  
y_train = t2m_scaler_train.transform(t2m_hr_train)
y_test = t2m_scaler_train.transform(t2m_hr_test)
y_val = t2m_scaler_train.transform(t2m_hr_val)

x_train = t2m_scaler_train.transform(t2m_lr_train)
x_test = t2m_scaler_train.transform(t2m_lr_test)
x_val = t2m_scaler_train.transform(t2m_lr_val)

z_scaler_train = dds.StandardScaler(axis=None)
z_scaler_train.fit(z_hr_train)  
y_z_train = z_scaler_train.transform(z_hr_train)
y_z_test = z_scaler_train.transform(z_hr_test)
y_z_val = z_scaler_train.transform(z_hr_val)

x_z_train = z_scaler_train.transform(z_lr_train)
x_z_test = z_scaler_train.transform(z_lr_test)
x_z_val = z_scaler_train.transform(z_lr_val)

##########3

y_train = y_train.expand_dims(dim='channel', axis=-1)
y_test = y_test.expand_dims(dim='channel', axis=-1)
y_val = y_val.expand_dims(dim='channel', axis=-1)

x_train = x_train.expand_dims(dim='channel', axis=-1)
x_test = x_test.expand_dims(dim='channel', axis=-1)
x_val = x_val.expand_dims(dim='channel', axis=-1)

y_z_train = y_z_train.expand_dims(dim='channel', axis=-1)
y_z_test = y_z_test.expand_dims(dim='channel', axis=-1)
y_z_val = y_z_val.expand_dims(dim='channel', axis=-1)

x_z_train = x_z_train.expand_dims(dim='channel', axis=-1)
x_z_test = x_z_test.expand_dims(dim='channel', axis=-1)
x_z_val = x_z_val.expand_dims(dim='channel', axis=-1)

########

ARCH_PARAMS = dict(n_filters=8,
                   n_blocks=8,
                   normalization=None,
                   dropout_rate=0.0,
                   dropout_variant='spatial',
                   attention=False,
                   activation='relu',
                   localcon_layer=True)

trainer = dds.SupervisedTrainer(
    backbone='resnet',
    upsampling='spc', 
    data_train=y_train, 
    data_val=y_val,
    data_test=y_test,
    data_train_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    data_val_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    data_test_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    scale=8,
    time_window=None, 
    static_vars=None,
    predictors_train=[y_z_train],
    predictors_val=[y_z_val],
    predictors_test=[y_z_test],
    interpolation='inter_area',
    patch_size=None, 
    batch_size=60, 
    loss='mae',
    epochs=100, 
    steps_per_epoch=None, 
    validation_steps=None, 
    test_steps=None, 
    learning_rate=(1e-3, 1e-4), lr_decay_after=1e4,
    early_stopping=False, patience=6, min_delta=0, 
    save=False, 
    save_path=None,
    show_plot=True, verbose=True, 
    device='GPU', 
    **ARCH_PARAMS)

trainer.run()

### ####################################################
### Inference

pred = dds.Predictor(
    trainer, 
    y_test, 
    scale=8, 
    array_in_hr=True,
    static_vars=None, 
    predictors=[y_z_test], 
    time_window=None,
    interpolation='inter_area', 
    batch_size=8,
    scaler=t2m_scaler_train,
    save_path=None,
    save_fname=None,
    return_lr=True,
    device='CPU')

unscaled_y_pred, coarsened_array = pred.run()

# ######################################
# Save

print (type(unscaled_y_pred), type (coarsened_array))
np.save('unscaled_y_pred.npy', unscaled_y_pred)
np.save('coarsened_array.npy', coarsened_array)
 
unscaled_y_test = t2m_scaler_train.inverse_transform(y_test)
np.save('unscaled_y_test.npy', unscaled_y_test)

print ("Finished")


#### Error:
...
SError: [Errno -51] NetCDF: Unknown file format: b'...
##### Solution:
conda install -c ioos netcdf4

https://github.com/Unidata/netcdf4-python/issues/509#issuecomment-172977395

### TF warnings
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
https://stackoverflow.com/questions/35911252/disable-tensorflow-debugging-information


### Error
ZeroDivisionError: integer division or modulo by zero
##### Possibly because 
failed call to cuInit: CUDA_ERROR_NO_DEVICE: no CUDA-capable device is detected
List of devices:
[]
Number of devices: 0
Global batch size: 0

##### Solution:
Try script on GPU


### GPU try
cd /scratch/fp0/mah900/empirical_downscaling/

qsub -I /home/900/mah900/conda/g1_h4.pbs
...
qsub: job 66706554.gadi-pbs ready

cd /scratch/fp0/mah900/empirical_downscaling/
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/empirical_downscaling 

python script-1.py 

### Error 
Still
List of devices:
[]
Number of devices: 0
Global batch size: 0
(Cuda not working)
##### Solution: 
check:
https://stackoverflow.com/questions/38009682/how-to-tell-if-tensorflow-is-using-gpu-acceleration-from-inside-python-shell

Install: https://stackoverflow.com/questions/38009682/how-to-tell-if-tensorflow-is-using-gpu-acceleration-from-inside-python-shell

##### Did not work: pip install tensorflow-gpu
pip install --upgrade --force-reinstall

##### Nexy try
Version: tensorflow-2.11.0
module load cuda/11.7.0 
module purge
pip uninstall tensorflow-gpu
Found existing installation: tensorflow-gpu 2.11.0

##### Find GPU suported version
https://www.tensorflow.org/install/source#gpu

#### istall
https://www.tensorflow.org/install/pip

conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/
python3 -m pip install tensorflow==2.11.0
(No work)
##### Next
module load cudnn/8.1.1-cuda11 
module load cuda/11.0.3
python -m pip install tensorflow==2.4
(Working)
#### Verify
python3 -c "import tensorflow as tf; print(tf.config.list_physical_devices('GPU'))"
(Found)
[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
 

#### Error 
ImportError: cannot import name 'sliding_window_view' from 'numpy.lib.stride_tricks'

Because of the version: https://github.com/twopirllc/pandas-ta/issues/285#issuecomment-833941155
#### Check version

pip show numpy
Name: numpy
Version: 1.19.5


pip install numpy==1.20.*

#### Error
ImportError: cannot import name 'TypeGuard' from 'typing_extensions' 

pip install typing-extensions --upgrade

### Error
AttributeError: module 'tensorflow.compat.v2.__internal__' has no attribute 'dispatch'

#### Solution:
Version issue 
https://stackoverflow.com/questions/67696519/module-tensorflow-compat-v2-internal-has-no-attribute-tf2
Reinstall TF to a newer version

#### Next, try
module load  cuda/11.2.2
module load cudnn/8.1.1-cuda11
python -m pip uninstall tensorflow
python -m pip install tensorflow==2.10
(GPU found)
(Training working)








#### Divide lines
https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

RED='\033[0;34m'; NC='\033[0m';printf "${RED}\n=================================================\n${NC}"
#### To reset the terminal colors, use the following command:
https://askubuntu.com/questions/443560/how-do-i-reset-the-colors-of-my-terminal
tput init
