### Create a gitlab repo

### 2023.02.07

https://gitlab.com/scratch-fp0-mah900/empirical_downscaling

### Initialize
cd /scratch/fp0/mah900/empirical_downscaling
git init
git add .
git commit -m "2023.02.07"

git remote add origin  git@gitlab.com:scratch-fp0-mah900/empirical_downscaling.git

git branch -M main
git push -uf origin main

#### Error 1
If wrong remote is added or trying to upadate remote

error: remote origin already exists.
#### Solution
https://www.cloudbees.com/blog/remote-origin-already-exists-error
git remote -v
git remote remove origin

#### Error 2
remote: You are not allowed to push code to this project.

#### Solution:

git remote add origin  git@gitlab.com:scratch-fp0-mah900/empirical_downscaling.git
 
#### Error 3
git push origin main

To gitlab.com:scratch-fp0-mah900/empirical_downscaling.git
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'gitlab.com:scratch-fp0-mah900/empirical_downscaling.git' 

#### Solution
git push -uf origin main