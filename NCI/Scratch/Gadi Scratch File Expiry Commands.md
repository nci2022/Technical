### Gadi Scratch File Expiry Commands

### Version: PUBLIC (20 Apr 2022)

https://nci.org.au/sites/default/files/documents/2022-04/GadiSystem-GadiScratchFileExpiryCommands-200422-1629-37.pdf


nci-file-expiry list-warnings 

nci-file-expiry list-quarantined 

nci-file-expiry recover

nci-file-expiry status


#### For list warning (1st data of month)

cd /home/900/mah900/scratch_recover
qsub  update_mah900.pbs 
qsub  update_shared.pbs
