### Command Line Interface (CLI)

https://docs.globus.org/cli/

Use only in an activated virtualenv.

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet
    pip install globus-cli


### Globus File Transfer Services
### Using the Globus CLI interface
https://www.nrel.gov/hpc/globus-file-transfer.html

$ globus login

<!--
5jgPuyJ8m5tm3Lwt7HeIi47hDpbO1I
-->

$ globus whoami
u1099516@anu.edu.au

$ globus endpoint search FourCastNet
ID                                   | Owner             | Display Name
------------------------------------ | ----------------- | ------------
945b3c9e-0f8c-11ed-8daf-9f359c660fbd | jpathak@nersc.gov | FourCastNet 

$ fourcast_ep=945b3c9e-0f8c-11ed-8daf-9f359c660fbd
$ globus ls ${fourcast_ep}
additional/
data/
model_weights/

$  globus endpoint activate ${fourcast_ep}
Endpoint is already activated. Activation expires at 2022-11-09 08:15:48+00:00

### Transfer Fourcast data

#### @gadi-login-04
    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet

#### Transfer a directory recursively:
https://docs.globus.org/cli/reference/transfer/

    $ fourcast_ep=945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    $ my_ep=85a9f386-5f53-11ed-b0b5-bfe7e7197080
    $ globus transfer ${fourcast_ep}:/data/ ${my_ep}:/scratch/fp0/mah900/globus_data/ --recursive

    Message: The transfer has been accepted and a task has been created and queued for execution
    Task ID: 983c622e-5f5b-11ed-8fd1-e9cb7c15c7d2

### Check on the status of the task. You could do this by
https://help.jasmin.ac.uk/article/4480-data-transfer-tools-globus-command-line-interface

    $ globus task show <taskid>

    $ globus task show  983c622e-5f5b-11ed-8fd1-e9cb7c15c7d2

    Label:                        None
    Task ID:                      983c622e-5f5b-11ed-8fd1-e9cb7c15c7d2
    Is Paused:                    False
    Type:                         TRANSFER
    Directories:                  10
    Files:                        83
    Status:                       ACTIVE
    Request Time:                 2022-11-08T11:50:56+00:00
    Faults:                       0
    Total Subtasks:               94
    Subtasks Succeeded:           11
    Subtasks Pending:             83
    Subtasks Retrying:            0
    Subtasks Failed:              0
    Subtasks Canceled:            0
    Subtasks Expired:             0
    Subtasks with Skipped Errors: 0
    Deadline:                     2022-11-09T11:50:56+00:00
    Details:                      OK
    Source Endpoint:              FourCastNet
    Source Endpoint ID:           945b3c9e-0f8c-11ed-8daf-9f359c660fbd
    Destination Endpoint:         my_ep1
    Destination Endpoint ID:      85a9f386-5f53-11ed-b0b5-bfe7e7197080
    Bytes Transferred:            11384913920
    Bytes Per Second:             35031969

### Task cancled after 1.5TB download, 
$ globus task cancel -v   983c622e-5f5b-11ed-8fd1-e9cb7c15c7d2
The task has been cancelled successfully.

====================================================================================
