
### Test 1

    $ . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
    $ conda activate /scratch/fp0/mah900/env/fourcastnet

    $ my_ep=85a9f386-5f53-11ed-b0b5-bfe7e7197080
    $ globus ls ${my_ep}

### Test 2
    $ ssh gadi-login-08
    $ [mah900@gadi-login-08 ~]$ /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -status

    Globus Online:   connected
    Transfer Status: idle

    /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -stop    

 ### Test 1, again

 $ globus ls ${my_ep}
    Globus CLI Error: A Transfer API Error Occurred.
    HTTP status:      502
    request_id:       gRiM0Q6b7
    code:             ExternalError.DirListingFailed.GCDisconnected
    message:          The Globus Connect Personal endpoint 'my_ep1 (85a9f386-5f53-11ed-b0b5-bfe7e7197080)' is not currently connected to Globus

### Copy Queue personal point start
qsub -I /home/900/mah900/conda/cqp_h10.pbs
...
[mah900@gadi-dm-02 ~]$ nohup /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -start &

[mah900@gadi-dm-02 ~]$ /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -status
Globus Online:   connected
Transfer Status: idle

========================================


