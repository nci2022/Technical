### How To Install, Configure, and Uninstall Globus Connect Personal for Linux

Needed to download file if you do not have a institute endpoint. 

https://docs.globus.org/how-to/globus-connect-personal-linux/

cd /scratch/fp0/mah900/globus

wget https://downloads.globus.org/globus-connect-personal/linux/stable/globusconnectpersonal-latest.tgz

tar xzf globusconnectpersonal-latest.tgz

cd globusconnectpersonal-3.2.0

### First run setup

./globusconnectpersonal
Detected that setup has not run yet, and '-setup' was not used
Will now attempt to run
  globusconnectpersonal -setup

Globus Connect Personal needs you to log in to continue the setup process.

We will display a login URL. Copy it into any browser and log in to get a
single-use code. Return to this command with the code to continue setup.

Login here:
-----
https://auth.globus.org/v2/oauth2/authorize?client_id=4d6448ae-8ca0-40e4-aaa9-8ec8e8320621&redirect_uri=https%3A%2F%2Fauth.globus.org%2Fv2%2Fweb%2Fauth-code&scope=openid+profile+urn%3Aglobus%3Aauth%3Ascope%3Aauth.globus.org%3Aview_identity_set+urn%3Aglobus%3Aauth%3Ascope%3Atransfer.api.globus.org%3Agcp_install&state=_default&response_type=code&code_challenge=5K-troHdnPJBKESNCcrRicUQ-FO_khZIgxeAyT-_aYc&code_challenge_method=S256&access_type=online&prefill_named_grant=gadi-login-05.gadi.nci.org.au
-----
Enter the auth code: 

======================

Provide a label for future reference: gadi-login-05.gadi.nci.org.au

Native App Authorization Code: 
ijFnwdD9YEp7LfiagfsvU1mlwC1JQ0
Authorization codes are valid for 10 minutes: this code will expire at 18:46.

==========================

Input a value for the Endpoint Name: gadi-01
registered new endpoint, id: 3d80ab92-5f38-11ed-8421-8d2923bcb3ec
setup completed successfully

============================
### Test

#### @gadi-login-08
  /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -setup 
  Input a value for the Endpoint Name: my_ep1
  registered new endpoint, id: 85a9f386-5f53-11ed-b0b5-bfe7e7197080
  setup completed successfully

  nohup /scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -start &
#### @gadi-login-05
ssh gadi-login-08
/scratch/fp0/mah900/globus/globusconnectpersonal-3.2.0/globusconnectpersonal  -status


### Managing Globus Connect Personal Directory Permissions via the Config File
https://docs.globus.org/how-to/globus-connect-personal-linux/

To configure which directories are accessible to Globus Connect Personal, edit the 
  ~/.globusonline/lta/config-paths 
file and restart Globus Connect Personal.

### Note
Sharing is a premium feature, so the endpoint must be covered by a subscription to use sharing. For additional details, see the sharing HowTo.

nano /home/900/mah900/.globusonline/lta/config-paths

  ~/,0,1
### To
  ~/,0,1
  /scratch/fp0/mah900/globus_data,1,1

### Data transfer tools: Globus Connect Personal
https://help.jasmin.ac.uk/article/5041-data-transfer-tools-globus-connect-personal

If successful, you should now be able to interact with your endpoint via any of the Globus tools (web app, CLI and Python SDK).

For example, you could list the files on the endpoint:

  $ globus ls <endpoint_id>:<path>

