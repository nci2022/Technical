### Containerization on Summit

To utilize a Docker container on Research Computing resources please build a singularity image using a Docker image as a base.


### Building Images Remotely with Singularity Hub
https://curc.readthedocs.io/en/iaasce-954_grouper/software/ContainerizationonSummit.html#building-images-remotely-with-singularity-hub

### Building Images Remotely with the Singularity Remote Builder
https://curc.readthedocs.io/en/iaasce-954_grouper/software/ContainerizationonSummit.html#building-images-remotely-with-the-singularity-remote-builder


### Building MPI-enabled Singularity images
https://curc.readthedocs.io/en/iaasce-954_grouper/software/ContainerizationonSummit.html#building-mpi-enabled-singularity-images

