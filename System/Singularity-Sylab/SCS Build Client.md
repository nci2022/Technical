### SCS Build Client

https://github.com/sylabs/scs-build-client

The SCS Build Client allows users to build Singularity Image Format (SIF) images via the Sylabs Cloud or Singularity Enterprise without the need to install and configure Singularity.