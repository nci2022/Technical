### Minicond-2022.09.14

rm -rf /scratch/fp0/mah900/Miniconda/*
cd /scratch/fp0/mah900/Miniconda

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

module load python3/3.10.4

bash /scratch/fp0/mah900/Miniconda/Miniconda3-latest-Linux-x86_64.sh -b -u -p /scratch/fp0/mah900/Miniconda/

Source: https://dev.to/waylonwalker/installing-miniconda-on-linux-from-the-command-line-4ad7

### Activate:
// . "/scratch/fp0/mah900/software2/miniconda/etc/profile.d/conda.sh"

. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

conda list


### Install mamba

conda install -n base mamba -c conda-forge
