### Getting Started with GPU Computing in Anaconda
https://www.anaconda.com/blog/getting-started-with-gpu-computing-in-anaconda

Some examples include:

* Linear algebra
* Signal and image processing (FFTs)
* Neural networks and deep learning
* Other machine learning algorithms, including 
* generalized linear models, gradient boosting, etc.
* Monte Carlo simulation and particle transport
* Fluid simulation
* In-memory databases
* … and the list gets longer every day


### GPU Accelerated Math Libraries: pyculib


* Linear algebra
* Fast Fourier Transforms
* Sparse Matrices
* Random number generation
* Sorting

### GPU Kernel Programming: Numba

### GPU Dataframes: PyGDF



