### How to use parallel ssh (PSSH) for executing commands in parallel on a number of Linux/Unix/BSD servers
https://www.cyberciti.biz/cloud-computing/how-to-use-pssh-parallel-ssh-program-on-linux-unix/

### PSSH – Execute commands on multiple Linux servers in parallel
https://www.2daygeek.com/pssh-parallel-ssh-run-execute-commands-on-multiple-linux-servers/