### Common Category Aggregation (CoCA) dataset

CoCA dataset consists of 80 categories with 1295 images, covering everyday indoor and outdoor scenes. It is worth noting that these categories are outright staggered with Microsoft COCO.

http://zhaozhang.net/coca.html