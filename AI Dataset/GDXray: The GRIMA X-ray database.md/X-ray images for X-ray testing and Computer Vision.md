
### X-ray images for X-ray testing and Computer Vision

As a service to the X-ray testing and Computer Vision communities, we collected more than 21.100 X-ray images for the development, testing, and evaluation of image analysis and computer vision algorithms. The images are organized in this public database called GDXray+: The GRIMA X-ray database (GRIMA is the name of our Machine Intelligence Group at the Department of Computer Science of the Pontificia Universidad Catolica de Chile).

https://domingomery.ing.puc.cl/material/gdxray/