### Distributed training
https://espnet.github.io/espnet/espnet2_distributed.html

### ESPnet: end-to-end speech processing toolkit
ESPnet is an end-to-end speech processing toolkit, mainly focuses on end-to-end speech recognition and end-to-end text-to-speech.

https://espnet.github.io/espnet/index.html
