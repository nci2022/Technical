### NVIDIA FLARE
https://developer.nvidia.com/flare

NVIDIA FLARE™ (NVIDIA Federated Learning Application Runtime Environment) is a domain-agnostic, open-source, and extensible SDK for Federated Learning. 
It allows researchers and data scientists to adapt existing ML/DL workflow to a federated paradigm and enables platform developers to build a secure, 
privacy-preserving offering for a distributed multi-party collaboration.




### NVIDIA / NVFlare
https://github.com/NVIDIA/NVFlare

### NVIDIA FLARE
https://nvflare.readthedocs.io/en/main/index.html


### Federated Learning With FLARE: NVIDIA Brings Collaborative AI to Healthcare and Beyond

https://blogs.nvidia.com/blog/2021/11/29/federated-learning-ai-nvidia-flare/


