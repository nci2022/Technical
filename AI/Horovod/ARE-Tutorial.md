
### ARE-Tutorial

### location
/scratch/fp0/mah900/HorovodTut

/scratch/fp0/mah900/HorovodTut/Page01-2.ipynb 

### From:
https://app.gitbook.com/s/968uMQ438pRA132VyaSv/

    /scratch/fp0/mah900/software2/miniconda/ 
    module load cuda/11.0.3
    /scratch/fp0/mah900/env/horovod2022
    
    horovodrun --check-build

### ARE
#### Compute Size: 
    gpus=8 cpus=96 mem=382G

#### Storage
    gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

#### Modules
    cuda/11.0.3

#### Python or Conda virtual environment base
    /scratch/fp0/mah900/software2/miniconda/

#### Conda environment
    /scratch/fp0/mah900/env/horovod2022


    SU estimate
    96 cpu cores + 382GB mem on gpuvolta queue (3 SUs/core/h) for 1h = 288 SUs


#### Patb
    /scratch/fp0/mah900/HorovodTut/Page01-2.ipynb 

    /scratch/fp0/mah900/HorovodTut/Page02.ipynb 