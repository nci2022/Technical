## ARE 

#### Compute Size
4GPU

#### Storage
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

#### Advanced
#### Modules
module load cuda/11.6.1

#### Python or Conda virtual environment base
/scratch/fp0/mah900/software2/miniconda/

#### Conda environment
/scratch/fp0/mah900/env/dataloader

#### Location 
/scratch/fp0/mah900/dali/Dataloader01.ipynb



