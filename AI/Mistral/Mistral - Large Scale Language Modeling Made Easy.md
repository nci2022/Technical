### Mistral - Large Scale Language Modeling Made Easy

Mistral combines Hugging Face 🤗, DeepSpeed, and Weights & Biases , with additional tools, helpful scripts, and documentation to facilitate:

* training large models with multiple GPU’s and nodes

* incorporating new pre-training datasets

* dataset preprocessing

* monitoring and logging of model training

* performing evaluation and measuring bias


https://nlp.stanford.edu/mistral/index.html