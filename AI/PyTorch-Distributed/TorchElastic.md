### torchelastic

If your train script works with torch.distributed.launch it will continue working with torchelastic.distributed.launch with these differences:


https://pytorch.org/elastic/0.2.0rc0/train_script.html