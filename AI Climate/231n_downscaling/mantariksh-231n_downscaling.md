### mantariksh / 231n_downscaling

On the other hand, data-based approaches could produce more general and convenient methods for downscaling. The current state-of-the-art deep learning architecture for climate data super-resolution is called Super-Resolution Convolutional Neural Net (SRCNN), used here.

https://github.com/mantariksh/231n_downscaling