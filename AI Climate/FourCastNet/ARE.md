### ARE, FourCastNet


https://opus.nci.org.au/x/3gCOCw


### Launch an ARE Instance:
Goto https://are.nci.org.au/ and login. 

In the Dashboard for JupyterLab launch, fill up the followings.
Walltime (hours):1
Queue: gpuvolta
Compute Size: 1gpu
Project: your own project (or fp0 to test).
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

In the Advanced options section: 
Modules: openmpi/4.1.4 
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda/
Conda environment: /scratch/fp0/mah900/env/fourcastnet
Pre-script: /g/data/wb00/admin/staging/FourCastNet_wb00/fourcastnetqp.sh


Now, Launch the JupyterLab Instance. 

### Launch the Notebook:
In the jupyter notebook, click view → open from path and inter the following location:
/home/fourcastnet_notebook/FourCastNet-01.ipynb
(Wait for the notebook to load)
All, codes are already present, no need to write anything.
(If you want to run the cells, must start from the top)
### Code and data location: 
The code and data are located in: /g/data/wb00/admin/staging



### My Fourcastnet notebook 
 cd /scratch/fp0/mah900/FourCastNet/
 /scratch/fp0/mah900/FourCastNet/FourCastNet-01.ipynb


 ### Install imagemagick
 http://louistiao.me/posts/notebooks/save-matplotlib-animations-as-gifs/

  . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
  conda activate /scratch/fp0/mah900/env/fourcastnet
  ### Conda: 
  https://anaconda.org/conda-forge/imagemagick

  conda install -c conda-forge imagemagick



### Made a copy to remove the video code. 
the  copy path : scratch/fp0/mah900/FourCastNet/FourCastNet-02.ipynb

