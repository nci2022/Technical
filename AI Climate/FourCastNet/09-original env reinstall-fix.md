### Original env reinstall/fix 

Fixing the first ("fourcastnet") env not the second env ("fourcastnet23")

#### Install missing packages (2023.02.02)
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/fourcastnet

python -m pip install jupyterlab
pip install jupyterlab_h5web

conda install -c conda-forge imagemagick

#### ARE - (Main parts unchanged)

Walltime (hours):1
Queue: gpuvolta
Compute Size: 1gpu
Project: your own project (or fp0 to test).
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

In the Advanced options section: 
Modules: openmpi/4.1.4 
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda/
Conda environment: /scratch/fp0/mah900/env/fourcastnet
#Romoved
#Pre-script: /g/data/wb00/admin/staging/FourCastNet_wb00/fourcastnetqp.sh

#changed
#/home/fourcastnet_notebook/FourCastNet-01.ipynb
g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet-01.ipynb

#### Notebook changed

#try:
...
#except ImportError:
#print ("Import Error: Reinstall missing packages")
  
#### More missing  
#### Error.1
ModuleNotFoundError: No module named 'wandb'
pip install wandb

#### Error.2
ModuleNotFoundError: No module named 'ruamel' 
pip install ruamel-yaml

#### Permissions
cd '/g/data/wb00/admin/staging/fourcastnet/'
chmod -R 755 .  # Was 775