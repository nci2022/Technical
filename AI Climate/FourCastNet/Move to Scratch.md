### Copy to scratch process

#### Done in 2022.11.22
Copy code to wb00
    cp -r /scratch/fp0/mah900/FourCastNet/  /g/data/wb00/admin/staging/FourCastNet_wb00

#### change pirmission
    cd /g/data/wb00/admin/staging
    chmod -R 775 FourCastNet_wb00/
    chmod -R 775 fourcastnet/

#### Copy qpfile 
    cp icenet_1/icenetqp.sh FourCastNet_wb00/fourcastnetqp.sh

#### Edit qpfile
nano fourcastnetqp.sh
mkdir -p  ${HOME}/fourcastnet_notebook
mkdir -p  ${HOME}/fourcastnet_notebook/FourCastNet_Fig
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet-01.ipynb  ${HOME}/fourcastnet_notebook/
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-1.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-1.png
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-2.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-2.png
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-3.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-3.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet-01.ipynb
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-1.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-2.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-3.png


### Change dir in the notebook 
Change dir to wb00