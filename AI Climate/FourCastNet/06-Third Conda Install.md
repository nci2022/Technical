### Third conda install FourCastNet
Try with new module and miniconda install:
1. module load python3/3.11.0 
2. new miniconda install at: 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/fourcastnet2 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet2

## Parallel H5Py install
module load gcc/12.2.0  hdf5  openmpi/4.1.3

Currently Loaded Modulefiles:
1) pbs   2) intel-mkl/2022.2.0   3) python3/3.11.0   4) gcc/12.2.0   5) hdf5/1.10.7(default)   6) openmpi/4.1.3 

#### Start with a fresh install
#https://docs.olcf.ornl.gov/software/python/parallel_h5py.html
#
#MPICC="mpicc -shared" pip install --no-binary=mpi4py mpi4py
#conda install -c defaults --override-channels numpy
#HDF5_MPI="ON" CC=mpicc pip install --no-binary=h5py h5py
(Does Not work)

### Next try
conda deactivate
rm -rf /scratch/fp0/mah900/env/fourcastnet2
module purge