### Custom Inference 2

### Using Env Install in 
/home/900/mah900/Technical/AI Climate/FourCastNet/07-Fourth Conda Install.md

### ARE 
https://are.nci.org.au/ 

Walltime (hours): 1
Queue:            gpuvolta
Compute Size:     1gpu
Project:          your own project (or fp0 to test).
Storage:          gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

Advanced options: 
Modules: openmpi/4.0.7 hdf5/1.12.2p 
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda2023
Conda environment: /scratch/fp0/mah900/env/fourcastnet23
#Pre-script: /g/data/wb00/admin/staging/FourCastNet_wb00/fourcastnetqp.sh

### Notebook load
scratch/fp0/mah900/FourCastNet/Inference.ipynb

### Install Missing
##### Activate
module load openmpi/4.0.7 hdf5/1.12.2p 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/fourcastnet23
##### Check
echo $CONDA_DEFAULT_ENV 
echo $CONDA_PREFIX
##### Install
pip install cdsapi netCDF4

### ARE has NO internet, 
but data is already download
otherwise, can launch manual JupyterLab


##### File 
scratch/fp0/mah900/FourCastNet/Inference.ipynb

