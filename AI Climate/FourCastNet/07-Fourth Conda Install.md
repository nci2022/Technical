### Fourth install Fourcastnet, 
### With 

rm -r /scratch/fp0/mah900/env/fourcastnet23
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23

#### Parallel HDF5 Install

https://docs.h5py.org/en/stable/build.html

"Be aware however that most pre-built versions lack MPI support, and that they are built against a specific version of HDF5. If you require MPI support, or newer HDF5 features, you will need to build from source"

### install python3-dev
If you use Anaconda, your python3-dev equivalent is already installed in $CONDA_PREFIX/lib/pythonX.Y where X.Y is the python version (example, 3.6).
https://askubuntu.com/questions/1073033/how-can-i-install-python3-dev-locally

### check if HDF5 is installed?
h5cc -showconfig:
https://unix.stackexchange.com/questions/287974/how-to-check-if-hdf5-is-installed

### Other source 
Installing parallel I/O libraries
https://www.pism.io/docs/installation/parallel-io-libraries.html

milancurcic/building_openmpi-hdf5-netcdf_stack.md
https://gist.github.com/milancurcic/3a6c1a97a99d291f88cc61dae6621bdf

### load MPICC and H5PCC

module load openmpi/4.1.4
mpicc --version

module load hdf5/1.12.2p

### Note it is h5pcc now
ls -lah /apps/hdf5/1.12.2p/bin/h5pcc
-rwxrwxr-x. 1 apps apps 12K Nov 21 11:40 /apps/hdf5/1.12.2p/bin/h5pcc

export CC=mpicc
export HDF5_MPI="ON"
export HDF5_DIR="/apps/hdf5/1.12.2p"  # If this isn't found by default
pip install --no-binary=h5py h5py

### Error -1 
...
File "/scratch/z00/mah900/tmp/pip-install-3ragrj47/h5py_a299c0fa690b4f5082ecb8df76a9f452/setup_configure.py", line 49, in validate_version
          raise ValueError(f"HDF5 version string {s!r} not in X.Y.Z format")
      ValueError: HDF5 version string '1.12.2p' not in X.Y.Z format
      [end of output]

### Next, Try conda parallel HDF5 (NOT WORK)
https://anaconda.org/clawpack/hdf5-parallel
conda install -c clawpack hdf5-parallel

### New try, with github

#### 1.
cd /scratch/fp0/mah900/
git clone https://github.com/h5py/h5py.git

#### 2.
nano /scratch/fp0/mah900/h5py/setup_configure.py 
line 46;
    m = re.match('(\d+)\.(\d+)\.(\d+)$', s[:-1])

#### 3.
cd /scratch/fp0/mah900/h5py
export CC=mpicc
export HDF5_MPI="ON"
pip install .

...
Successfully installed h5py-3.7.0 mpi4py-3.1.4 numpy-1.24.1

### Test 
nano demo2.py

from mpi4py import MPI
import h5py

rank = MPI.COMM_WORLD.rank  # The process ID (integer 0-3 for 4-process run)

f = h5py.File('parallel_test.hdf5', 'w', driver='mpio', comm=MPI.COMM_WORLD)

dset = f.create_dataset('test', (4,), dtype='i')
dset[rank] = rank

f.close()

mpiexec -n 4 python demo2.py

### Error 2
    from mpi4py import MPI
ImportError: /scratch/fp0/mah900/env/fourcastnet23/lib/python3.8/site-packages/mpi4py/MPI.cpython-38-x86_64-linux-gnu.so: undefined symbol: ompi_mpi_logical8

#### Tried, (NOT Worked)
https://stackoverflow.com/questions/55129738/centos-7-undefined-symbol-ompi-mpi-logical8
pip uninstall mpi4py
pip install --no-cache-dir mpi4py

#### Tried, (Not worked)
pip uninstall mpi4py

https://block2.readthedocs.io/en/latest/user/installation.html#installation-with-anaconda

python -m pip install --no-binary :all: mpi4py

#### Tried (worked)
https://stackoverflow.com/a/61955682
conda install mpi4py  --force-reinstall

### Error 3 
 import h5py
  File "/scratch/fp0/mah900/h5py/h5py/__init__.py", line 29, in <module>
    raise ImportError("You cannot import h5py from inside the install directory.\nChange to another directory first.")
ImportError: You cannot import h5py from inside the install directory.

#### Tried, 
moving out

mv /scratch/fp0/mah900/h5py/demo2.py /scratch/fp0/mah900/FourCastNet/

### Test, Again

cd /scratch/fp0/mah900/FourCastNet/
mpiexec -n 4 python demo2.py

### Error
 undefined symbol: H5Pget_virtual_count
##### Tried (NOT worked)
https://stackoverflow.com/questions/73420378/error-when-importing-h5-h5py-on-conda-undefined-symbol-h5pget-fapl-direct

cd /scratch/fp0/mah900/h5py
pip uninstall h5py
export CC=mpicc
export HDF5_MPI="ON"
pip install .

#### Tried (Not worked)
cd /scratch/fp0/mah900/h5py
pip uninstall h5py
export CC=mpicc
export HDF5_MPI="ON"
pip install --no-cache-dir h5py


#### Tried, (worked)
https://stackoverflow.com/questions/58345261/configuring-h5py-with-mpio-using-anaconda-on-windows

conda install -c conda-forge "h5py>=2.9=mpi*"

https://github.com/conda-forge/h5py-feedstock/issues/44
For posterity, the most graceful way to install the h5py mpi build is

conda install "h5py>=2.9=mpi*"  # 2.9 is the first version built with mpi on this channel
because the h5py build recipe does not track features. This was intentional; the h5py mpi build will never be installed automatically even if mpi is already installed.

#### Test 2

nano demo.py

from mpi4py import MPI
print("Hello World (from process %d)" % MPI.COMM_WORLD.Get_rank())

mpiexec -n 4 python demo.py

### Error
all showing rank 0

##### Solution 
https://stackoverflow.com/questions/4039608/why-do-all-my-open-mpi-processes-have-rank-0#:~:text=This%20is%20almost%20always%20a,the%20one%20distribution%20in%20them.

This is almost always a result of compiling with one MPI distribution and (accidentally or otherwise) running it with the mpirun of another. Make sure all your paths and dynamic library search paths have only the one distribution in them. 

#### mpich version
https://stackoverflow.com/questions/17356765/how-do-i-check-the-version-of-mpich
$  mpichversion
MPICH Version:    	3.3.2

$ mpiexec --version
mpiexec (OpenRTE) 4.1.4

### Tried (NOT work)
conda list
...
mpich   

conda uninstall mpich

#### Try again

rm -r /scratch/fp0/mah900/env/fourcastnet23
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23

##module load openmpi/4.1.4 (NOT needed)
##module load hdf5/1.12.2p (NOT needed)

##export CC=mpicc (NOT needed)
##export HDF5_MPI="ON" (NOT needed)

conda install -c conda-forge "h5py>=2.9=mpi*"

### Test 
https://docs.h5py.org/en/stable/mpi.html
cd /scratch/fp0/mah900/FourCastNet/

 mpiexec -n 4 python demo.py
Hello World (from process 3)
Hello World (from process 0)
Hello World (from process 2)
Hello World (from process 1)

### Error
 mpicc --version
/scratch/fp0/mah900/env/fourcastnet23/bin/mpicc: line 285: x86_64-conda_cos6-linux-gnu-cc: command not found

#### Tried:
https://github.com/RcppCore/Rcpp/issues/770
conda install gxx_linux-64


##### Latest Working version (2023/01/19)
conda install -c conda-forge "h5py>=3.2=mpi*" (mpiexec:  unexpected error - no non-PBS mpiexec in PATH)

conda install -c conda-forge "h5py>=3.7=mpi*" (mpiexec:  unexpected error - no non-PBS mpiexec in PATH)

### Working 
 conda install -c conda-forge "h5py=*=mpi*"

 It's not the most convenient, but you have to specify an mpi build of hdf5 to be installed, e.g. conda install mpich hdf5=*=mpi*. We could make this more convenient by having an empty hdf5-mpi metapackage that just depends on the mpi builds so users don't have to do the build string bit.
 https://github.com/conda-forge/hdf5-feedstock/issues/118#issuecomment-549711541


### Summary of Parallel H5Py Install

rm -r /scratch/fp0/mah900/env/fourcastnet23
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23

conda install -c conda-forge "h5py=*=mpi*"


### Modulus Install

conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

#### Error 
Conda H5py, Version not compatible 

#### Next try, H5Py

#### 1
https://drtiresome.com/2016/08/23/build-and-install-mpi-parallel-hdf5-and-h5py-from-source-on-linux/
#conda install -c conda-forge mpich
#Can not find mpicc

#### 2
#https://github.com/conda-forge/openmpi-feedstock/issues/34#issuecomment-461832148
#conda install openmpi-mpicc
#Does not work with mpi4py install

#### 3
module load openmpi/4.1.4 
pip install mpi4py

cd /scratch/fp0/mah900/hdf5/
wget https://hdf-wordpress-1.s3.amazonaws.com/wp-content/uploads/manual/HDF5/HDF5_1_14_0/src/hdf5-1.14.0.tar.gz
tar -xvf hdf5-1.14.0.tar.gz

cd /scratch/fp0/mah900/hdf5/hdf5-1.14.0
 ./configure --enable-parallel --enable-shared --prefix=/scratch/fp0/mah900/hdf5 

### Error
compiler 'g++' is GNU g++-8.5.0
No match to get cc_version_info for /scratch/fp0/mah900/env/fourcastnet23/bin/x86_64-conda_cos6-linux-gnu-cc

#### Tried
https://github.com/RcppCore/Rcpp/issues/770
conda install gxx_linux-64

### Error
checking whether a simple MPI-IO C program can be linked... no
configure: error: unable to link a simple MPI-IO C program

### Not worked

### Next try


### Source installation
https://docs.h5py.org/en/stable/build.html#source-installation

rm -r /scratch/fp0/mah900/env/fourcastnet23
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"

conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23

python3 -m pip install python-dev-tools --user --upgrade
https://pypi.org/project/python-dev-tools/

module load  openmpi/4.1.4  hdf5/1.12.2p  gcc/12.2.0

echo $HDF5_DIR
/apps/hdf5/1.12.2p

export HDF5_MPI=ON
export HDF5_VERSION=1.12.2p
export CC=mpicc

cd /scratch/fp0/mah900/hdf5
git clone https://github.com/h5py/h5py.git

cd /scratch/fp0/mah900/hdf5/h5py
nano -c setup_configure.py 
line 46: s[:-1]

pip install -v .

...
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
thinc 8.0.15 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, which is not installed.
spacy 3.1.1 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, which is not installed.
spacy 3.1.1 requires tqdm<5.0.0,>=4.38.0, which is not installed.
pandas 1.3.1 requires python-dateutil>=2.7.3, which is not installed.
spacy 3.1.1 requires typer<0.4.0,>=0.3.0, but you have typer 0.4.2 which is incompatible.
Successfully installed h5py-3.7.0 mpi4py-3.1.4 numpy-1.24.1


### Test 

cd /scratch/fp0/mah900/FourCastNet/

 mpiexec -n 4 python demo.py
 mpiexec -n 4 python demo2.py

### Error
h5dump: error while loading shared libraries: libmpi.so.12: cannot open shared object file: No such file or directory
Problem is with hdf5/1.12.2p


### Next try
#### Clean
conda deactivate
module purge
rm -r /scratch/fp0/mah900/env/fourcastnet23
#### Env create
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23
#### Development headers
python3 -m pip install python-dev-tools --user --upgrade

### Install Missing packages
#pip install python-dateutil
#pip install pydantic
#pip install tqdm

#### MPI and Parallel HDF5
#module load openmpi/4.1.3
module load openmpi/4.1.4
#module load intel-mpi/2021.7.0
module load hdf5/1.12.1p 
module load gcc/12.2.0
#### H5Py git
cd /scratch/fp0/mah900/software23
git clone https://github.com/h5py/h5py.git
cd /scratch/fp0/mah900/software23/h5py
nano -c +46 setup_configure.py 
line 46: s[:-1]
#### ENV variables and Install
export HDF5_MPI=ON
export HDF5_VERSION=1.12.1p
export CC=mpicc
pip install -v .

#### Error 1
  Loading library to get build settings and version: libhdf5.so
  error: Unable to load dependency HDF5, make sure HDF5 is installed properly
  error: libhdf5.so: cannot open shared object file: No such file or directory
  error: subprocess-exited-with-error
#### Tried
  Change 
  export CC=gcc
  To
  export CC=mpicc

### Modules Compability check
 module purge
#module load openmpi/4.1.3 hdf5/1.10.7p (Does not match)
#module load openmpi/4.1.2 hdf5/1.12.1p (Does not match)
#module load openmpi/4.1.1 hdf5/1.12.1p (Does not match)
module load openmpi/4.1.0 hdf5/1.12.1p  (Does not match)
module load openmpi/4.0.1  hdf5/1.12.1p (Does not match)
module load openmpi/4.0.7  hdf5/1.12.1p (Does not match)
module load openmpi/4.1.0 hdf5/1.12.2p  (Testing error, [LOG_CAT_ML] ml_discover_hierarchy exited with error)
module load openmpi/4.0.7  hdf5/1.12.2p (only hdf5/1.12.2p works, 
                                          and does not produce error while testing)

### Next, Try (2022.01.20)

#### Clean
conda deactivate
module purge
rm -r /scratch/fp0/mah900/env/fourcastnet23
#### Env create
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/fourcastnet23 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet23
#### Development headers
python3 -m pip install python-dev-tools --user --upgrade

#### MPI and Parallel HDF5
module load openmpi/4.0.7 hdf5/1.12.2p 
 
#### H5Py git
cd /scratch/fp0/mah900/software23
git clone https://github.com/h5py/h5py.git
cd /scratch/fp0/mah900/software23/h5py
nano -c +46 setup_configure.py 
line 46: s[:-1]
#### ENV variables and Install
cd /scratch/fp0/mah900/software23/h5py
export HDF5_MPI=ON
export HDF5_VERSION=1.12.1p
export CC=mpicc
pip install -v .
 
####
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
thinc 8.0.15 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, which is not installed.
spacy 3.1.1 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, which is not installed.
spacy 3.1.1 requires tqdm<5.0.0,>=4.38.0, which is not installed.
pandas 1.3.1 requires python-dateutil>=2.7.3, which is not installed.
spacy 3.1.1 requires typer<0.4.0,>=0.3.0, but you have typer 0.4.2 which is incompatible.
Successfully installed h5py-3.7.0 mpi4py-3.1.4 numpy-1.24.1


#### Test 
cd /scratch/fp0/mah900/FourCastNet/
mpiexec -n 4 python demo.py
mpiexec -n 4 python demo2.py 

#### Then, to Modulus
conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

pip3 install matplotlib transforms3d future typing  quadpy\
             numpy-stl==2.16.3  sympy==1.5.1 termcolor psutil\
             symengine==0.6.1 numba Cython chaospy torch_optimizer\
             vtk chaospy termcolor omegaconf hydra-core==1.1.1 einops\
             timm tensorboard pandas orthopy ndim functorch pint

conda install -c conda-forge git-lfs
git lfs install    

cd /scratch/fp0/mah900/modulus/
python setup.py install     

### Test with GPU (Needs GPU)
    qsub -I /home/900/mah900/conda/g1_h2.pbs
 
    module load openmpi/4.0.7 hdf5/1.12.2p 
    . "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
    conda activate /scratch/fp0/mah900/env/fourcastnet23
    cd /scratch/fp0/mah900/modulus_examples/helmholtz/
    python helmholtz.py

#### Warning 
/scratch/fp0/mah900/env/fourcastnet23/lib/python3.8/site-packages/h5py/__init__.py:36: UserWarning: h5py is running against HDF5 1.12.2 when it was built against 1.12.1, this may cause problems
  _warn(("h5py is running against HDF5 {0} when it was built against {1}, "
#### Tried (No luck)
module unload hdf5/1.12.2p 
module   load hdf5/1.12.1p 

### Install jupyterlab
  module load openmpi/4.0.7 hdf5/1.12.2p 
  . "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
  conda activate /scratch/fp0/mah900/env/fourcastnet23
  python -m pip install jupyterlab
  pip install jupyterlab_h5web

### ARE
https://are.nci.org.au/ 

Walltime (hours): 1
Queue:            gpuvolta
Compute Size:     1gpu
Project:          your own project (or fp0 to test).
Storage:          gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

Advanced options: 
Modules: openmpi/4.0.7 hdf5/1.12.2p 
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda2023
Conda environment: /scratch/fp0/mah900/env/fourcastnet23
#Pre-script: /g/data/wb00/admin/staging/FourCastNet_wb00/fourcastnetqp.sh

### My Fourcastnet notebook 
 cd /scratch/fp0/mah900/FourCastNet/
 /scratch/fp0/mah900/FourCastNet/FourCastNet-01.ipynb

 ### Install imagemagick for Animation
conda install -c conda-forge imagemagick

#### Missing, Installed   
##### Activate
module load openmpi/4.0.7 hdf5/1.12.2p 
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/fourcastnet23
##### Check
echo $CONDA_DEFAULT_ENV 
echo $CONDA_PREFIX
##### Install
pip install wandb ruamel.yaml timm  einops --upgrade tqdm 

 




