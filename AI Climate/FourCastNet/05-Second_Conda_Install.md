### Second Conda Install

. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/fourcastnet2 python=3.8
conda activate /scratch/fp0/mah900/env/fourcastnet2

conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

pip3 install matplotlib transforms3d future typing numpy quadpy\
             numpy-stl==2.16.3 h5py sympy==1.5.1 termcolor psutil\
             symengine==0.6.1 numba Cython chaospy torch_optimizer\
             vtk chaospy termcolor omegaconf hydra-core==1.1.1 einops\
             timm tensorboard pandas orthopy ndim functorch pint

conda install -c conda-forge git-lfs
git lfs install

cd /scratch/fp0/mah900/modulus/
python setup.py install

#### Test
cd /scratch/fp0/mah900/modulus_examples/helmholtz/
python helmholtz.py

module load openmpi/4.1.4 

### Parallel HDF5 (Oak Ridge)
https://docs.olcf.ornl.gov/software/python/parallel_h5py.html

 gcc --version
gcc (GCC) 8.5.0 20210514 (Red Hat 8.5.0-10)
Copyright (C) 2018 Free Software Foundation, Inc.

 mpicc --version
gcc (GCC) 8.5.0 20210514 (Red Hat 8.5.0-10)
Copyright (C) 2018 Free Software Foundation, Inc.

>>> import h5py
>>> print(h5py.__file__)
/scratch/fp0/mah900/env/fourcastnet2/lib/python3.8/site-packages/h5py/__init__.py

#### Install
#MPICC="mpicc -shared" pip install --no-binary=mpi4py mpi4py
HDF5_MPI="ON" CC=mpicc pip install --force --no-binary=h5py h5py

#### Error 
Loading library to get build settings and version: libhdf5.so
      error: Unable to load dependency HDF5, make sure HDF5 is installed properly
      error: libhdf5.so: cannot open shared object file: No such file or directory

### Solution
module load hdf5
HDF5_MPI="ON" CC=mpicc pip install --force --no-binary=h5py h5py


### Jupyter Lab
mamba install -c conda-forge jupyterlab



### Run from login node
module load openmpi/4.1.4  hdf5
jupyter lab --no-browser --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
--notebook-dir="/scratch/fp0/mah900/FourCastNet"

#### Connect
ssh -N -v -L   8890:gadi-login-09.gadi:8890 mah900@gadi.nci.org.au  

### Error 
ImportError: /scratch/fp0/mah900/env/fourcastnet2/lib/python3.8/site-packages/h5py/defs.cpython-38-x86_64-linux-gnu.so: undefined symbol: H5Pget_dxpl_mpio

### Solution, tried
pip uninstall h5py
module purge

module load openmpi/4.1.4 
module load hdf5/1.12.2
HDF5_MPI="ON" CC=mpicc pip install --no-binary=h5py h5py
### Not working 

 module load openmpi/4.1.4
 conda install -c conda-forge "h5py>=2.9=mpi*"
### Error



### Error
  File "/scratch/fp0/mah900/env/fourcastnet2/lib/python3.8/site-packages/h5py/__init__.py", line 25, in <module>
    from . import _errors
ImportError: libmpi.so.12: cannot open shared object file: No such file or directory
 

### Solution 
https://stackoverflow.com/questions/69930526/importerror-libmpi-so-12-cannot-open-shared-object-file-no-such-file-or-direc

penmpi/4.1.4(default)   2) hdf5/1.12.2  

Create a symbolic link: 
cd /scratch/fp0/mah900/env/fourcastnet2/lib
ln -s /apps/openmpi/4.1.4/lib/libmpi.so.40.30.4  libmpi.so.12
(Working)

### Error
 ModuleNotFoundError: No module named 'wandb'
ModuleNotFoundError: No module named 'ruamel' 
ModuleNotFoundError: No module named 'cdsapi'
### Solution
pip install wandb
pip install ruamel-yaml

### Error 
ImportError: /scratch/fp0/mah900/env/fourcastnet2/lib/python3.8/site-packages/mpi4py/MPI.cpython-38-x86_64-linux-gnu.so: undefined symbol: MPI_Aint_add
### Solution
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/scratch/fp0/mah900/Miniconda/lib/ (DID NOT WORK)

### working 
https://stackoverflow.com/questions/55129738/centos-7-undefined-symbol-ompi-mpi-logical8
pip uninstall mpi4py
env MPICC=/apps/openmpi/4.1.4/bin/mpicc pip install --no-cache-dir mpi4py


### TRY again 

#### Update 
conda deactivate
#conda update conda (NOT Work)
conda update -n base -c defaults conda (NOT work)

Have to reinstall miniconda







