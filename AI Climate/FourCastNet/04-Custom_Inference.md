### Custom Inference 

### Document 
https://github.com/NVlabs/FourCastNet#inference-for-a-custom-interval


### Login node setup 
cd '/scratch/fp0/mah900/FourCastNet'
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/fourcastnet2

### Run JupyterLab
https://stackoverflow.com/questions/41159797/how-to-disable-password-request-for-a-jupyter-notebook-session

https://stackoverflow.com/questions/35254852/how-to-change-the-jupyter-start-up-folder

#jupyter notebook --no-browser --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
#--notebook-dir="/scratch/fp0/mah900/FourCastNet"

jupyter lab --no-browser --ip='*' --NotebookApp.token='' --NotebookApp.password='' \
#--notebook-dir="/scratch/fp0/mah900/FourCastNet"


### Connect 
https://www.ssh.com/academy/ssh/tunneling-example#local-forwarding

ssh -N -v-L   8889:gadi-login-05.gadi:8889 mah900@gadi.nci.org.au  

### Created New page
temp.ipynb

### Error 1
ModuleNotFoundError: No module named 'cdsapi'

### Solution
https://pypi.org/project/cdsapi/

pip install cdsapi

cat ~/.cdsapirc
url: https://cds.climate.copernicus.eu/api/v2
key: 164095:27f7dc1a-bce4-4d5e-936f-be72557d67c2

### Down load

/g/data/wb00/admin/staging/fourcastnet/cds/oct_2021_19_31_pl.nc

'/g/data/wb00/admin/staging/fourcastnet/cds/oct_2021_19_31_sfc.nc'

dest = '/g/data/wb00/admin/staging/fourcastnet/cds/oct_2021_19_21.h5'

### Error 2
ImportError: libmpi.so.40: cannot open shared object file: No such file or directory

### Solution
https://github.com/theislab/cellrank/issues/864#issuecomment-1184279494

conda install -c conda-forge openmpi=4.1.4=ha1ae619_100

### Error 3
ModuleNotFoundError: No module named 'netCDF4'
### Solution
pip install netCDF4


### Error 4
ValueError: h5py was built without MPI support, can't use mpio driver
### Solution
https://stackoverflow.com/a/59073514
https://docs.h5py.org/en/stable/mpi.html

conda install -c conda-forge "h5py>=2.9=mpi*" (DID NOT work)


### Parallel HDF5 (www.olcf.ornl.gov)
https://docs.olcf.ornl.gov/software/python/parallel_h5py.html

$ module load gcc
$ module load hdf5 

pip uninstall  mpi4py
MPICC="mpicc -shared" pip install --no-binary=mpi4py mpi4py

### Error 
The Open MPI wrapper compiler was unable to find the specified compiler
      x86_64-conda-linux-gnu-cc in your PATH.
### Solution
https://github.com/conda-forge/openmpi-feedstock/issues/34#issuecomment-461832148

conda install openmpi-mpicc  (Did not work) 
module load openmpi/4.0.2  (Working ...)

### Next
conda install -c defaults --override-channels numpy

pip uninstall h5py
HDF5_MPI="ON" CC=mpicc pip install --no-binary=h5py h5py

### Error 
ERROR: Could not build wheels for h5py, which is required to install pyproject.toml-based projects

### Next, try (Working)
module purge
 module load hdf5/1.10.7p

### Error
Uninstalled conda hdf5, now system is broke
All needs reinstalling

module load openmpi
pip install mpi4py

### NOT WORKING, WILL TRY a Install


