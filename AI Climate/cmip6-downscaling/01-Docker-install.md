###  Docker convert

#### Sylabs login
goto https://cloud.sylabs.io/builder
login using gitlab:nci2022

##### cmip6 downscaling Docker hub
https://hub.docker.com/r/carbonplan/cmip6-downscaling-single-user/tags

### New Key (if needed)
### Remote Endpoints
##### https://docs.sylabs.io/guides/latest/user-guide/endpoint.html

$ module load singularity 
$ singularity remote status
...
Authentication token is invalid (please login again).
FATAL:   error response from server: Invalid Credentials


Goto: https://cloud.sylabs.io/dashboard
##### New Token steps, follow below
https://docs.sylabs.io/guides/latest/user-guide/endpoint.html#public-sylabs-cloud

$ singularity remote status
...
INFO:    Access Token Verified!

### The remote builder
https://supercontainers.github.io/sc19-tutorial/04-hands-on.html


[mah900@gadi-login-05 cmip6-downscaling]$  singularity build --remote cmip6-downscaling-single-user.sif  docker:carbonplan/cmip6-downscaling-single-user:latest

##### Watch build 
https://cloud.sylabs.io/builder


### Run
https://singularityhub.github.io/singularity-hpc/r/jupyter-scipy-notebook/


### Try 1 (Error connecting)
cd /scratch/fp0/mah900/cmip6-downscaling
singularity exec cmip6-downscaling-single-user.sif jupyter notebook --no-browser --port=$(shuf -i 2000-65000 -n 1)  
...
[I 15:35:36.226 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 15:35:36.226 NotebookApp] http://localhost:47400/

### Try 2 (Working)
### How to disable password request for a Jupyter notebook session?
https://stackoverflow.com/questions/41159797/how-to-disable-password-request-for-a-jupyter-notebook-session


cd /scratch/fp0/mah900/cmip6-downscaling
singularity exec cmip6-downscaling-single-user.sif jupyter notebook --no-browser --port=$(shuf -i 2000-65000 -n 1)  --ip='*' --NotebookApp.token='' --NotebookApp.password=''
...
[I 16:17:35.588 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 16:17:35.588 NotebookApp] http://gadi-login-02.gadi.nci.org.au:2321/

### Port Forwarding
https://www.ssh.com/academy/ssh/tunneling-example

 ssh -v -N -L 2321:gadi-login-02:2321 gadi.nci.org.au

 =================================================================