
### Docker run on GPU

#### GPU allocation
qsub -I /home/900/mah900/conda/g1_h2.pbs 
...
[mah900@gadi-gpu-v100-0089 ~]$ 

#### Run
##### sigularity CUDA support
https://docs.sylabs.io/guides/3.5/user-guide/gpu.html

$ cd /scratch/fp0/mah900/cmip6-downscaling
$ module load singularity 
$ module load cuda


#### Inspect (working)

singularity shell --nv cmip6-downscaling-single-user.sif

### Run Jupyter


$ singularity exec --nv cmip6-downscaling-single-user.sif jupyter notebook --no-browser --port=$(shuf -i 2000-65000 -n 1)  --ip='*' --NotebookApp.token='' --NotebookApp.password=''


### Port Forwarding
PORT=4437
HOST=gadi-gpu-v100-0089
ssh -v -N -L $PORT:$HOST:$PORT gadi.nci.org.au


### Error: 
Tensorflow-io not found

================================
### CPU

$ cd /scratch/fp0/mah900/cmip6-downscaling
$ module load singularity 
$ singularity exec  cmip6-downscaling-single-user.sif jupyter notebook --no-browser --port=$(shuf -i 2000-65000 -n 1)  --ip='*' --NotebookApp.token='' --NotebookApp.password=''

### Same Error: can not find any modules

### CPU
$ cd /scratch/fp0/mah900/cmip6-downscaling
$ module load singularity 
$ singularity shell cmip6-downscaling-single-user.sif 

### Conda inside singularity
Singularity> source /srv/conda/etc/profile.d/conda.sh 
Singularity> conda activate /srv/conda/envs/notebook
(notebook) Singularity> 

Singularity> /usr/bin/which python
/srv/conda/envs/notebook/bin/python

### Does not work tensorflow/Dask missing
============================================

