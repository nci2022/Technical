### Try 1
    
    cd /scratch/fp0/mah900

    git clone https://github.com/carbonplan/cmip6-downscaling.git

    . "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

### update
    conda update -n base -c defaults conda

### Inside env, use subfolder or everything will be overwritten 
    conda create -p /scratch/fp0/mah900/env/cmip6-downscaling python=3.8

### ERROR: Ignored the following versions that require a different python version: 0.0.0 Requires-Python >=3.8; 0.1.11 Requires-Python >=3.8    
    # conda create -p /scratch/fp0/mah900/env/cmip6-downscaling python=3.7


### Activate
    conda activate /scratch/fp0/mah900/env/cmip6-downscaling

    cd /scratch/fp0/mah900/cmip6-downscaling
    python -m pip install cmip6_downscaling


### ERROR: dependency conflicts.
    typer 0.3.2 requires click<7.2.0,>=7.1.1, but you have click 8.1.3 which is incompatible.

    thinc 8.0.15 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, but you have pydantic 1.10.2 which is incompatible.

    spacy 3.1.1 requires pydantic!=1.8,!=1.8.1,<1.9.0,>=1.7.4, but you have pydantic 1.10.2 which is incompatible.



### jupyterLab
### ARE can't find kernel
    pip install jupyterlab

### ARE
    /scratch/fp0/mah900/Miniconda/
    /scratch/fp0/mah900/env/cmip6-downscaling

### JupyterLab  with conda  
    conda install jupyterlab

### Jupyter conda install - check    
    !echo $CONDA_PREFIX
    /scratch/fp0/mah900/env/cmip6-downscaling

### Change directory
    %pwd  #look at the current work dir
    %cd   #change to the dir you want 
    https://stackoverflow.com/questions/15680463/change-ipython-jupyter-notebook-working-directory

    %cd /scratch/fp0/mah900/cmip6-downscaling
    %pwd