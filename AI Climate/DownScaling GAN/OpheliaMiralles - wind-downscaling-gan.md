### OpheliaMiralles / wind-downscaling-gan

https://github.com/OpheliaMiralles/wind-downscaling-gan

#### Wind downscaling over Switzerland project (EPFL/UNIBE)

ERA5 reanalysis from ECMWF: global gridded dataset from 1979 - today with a spatial resolution of 25 x 25 km and a 1-hourly temporal resolution


#### Ophelia Miralles
@OpheliaMiralles
https://github.com/OpheliaMiralles

