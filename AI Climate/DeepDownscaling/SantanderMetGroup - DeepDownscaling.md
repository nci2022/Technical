### SantanderMetGroup / DeepDownscaling

https://github.com/SantanderMetGroup/DeepDownscaling


### On the suitability of deep convolutional neural networks for continental-wide downscaling of climate change projections

This notebook is written in the free programming language R

CMIP6

https://github.com/SantanderMetGroup/DeepDownscaling/blob/master/2020_Bano_CD.ipynb