### annavaughan / convCNPClimate

Implementation of convolutional conditional neural processes for statistical downscaling following https://gmd.copernicus.org/preprints/gmd-2020-420/.

https://github.com/annavaughan/convCNPClimate