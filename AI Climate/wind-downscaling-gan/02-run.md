### Run-Jupyter
(Installed from 01)

cd /scratch/fp0/mah900/wind-downscaling-gan
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
#conda activate /scratch/fp0/mah900/env/wind-downscaling-gan
conda activate downscale_dev
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

##### Shows IP differently
jupyter lab --no-browser --port=$(shuf -i 2000-65000 -n 1)  --ip='*' --NotebookApp.token='' --NotebookApp.password=''
#### command not found
jupyter notebook --no-browser --port=$(shuf -i 2000-65000 -n 1) --ip='*' --NotebookApp.token='' --NotebookApp.password=''

#### Port Forwarding
PORT=4437
HOST=gadi-login-07
ssh -v -N -L $PORT:$HOST:$PORT gadi.nci.org.au


### ARE (working)
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

cudnn/8.1.1-cuda11 cuda/11.0.3
/scratch/fp0/mah900/Miniconda/
downscale_dev

#### Notebook

##### Test
https://stackoverflow.com/questions/36539623/how-do-i-find-the-name-of-the-conda-environment-in-which-my-code-is-running

!which python
/scratch/fp0/mah900/Miniconda/envs/downscale_dev/bin/python
!echo $CONDA_DEFAULT_ENV
!echo $CONDA_PREFIX

##### Test 2
import os
from datetime import date, datetime
from pathlib import Path
from downscaling.data import download_ERA5
from downscaling import downscale, plot_wind_fields
import xarray as xr
from downscaling import process_era5, process_topo, predict
import requests
from downscaling import plot_elevation, plot_wind_fields
from cartopy.crs import epsg

##### Error
ModuleNotFoundError: No module named 'silence_tensorflow'
##### Solution
https://copypaste.guru/WhereIsMyPythonModule/how-to-fix-modulenotfounderror-no-module-named-silence-tensorflow

https://www.roseindia.net/answers/viewqa/pythonquestions/220292-ModuleNotFoundError-No-module-named-silence-tensorflow.html

    pip install silence-tensorflow

#### Error
ModuleNotFoundError: No module named 'parse'
##### Solution
https://pypi.org/project/parse/

pip install parse

#### Error
ModuleNotFoundError: No module named 'topo_descriptors'
###### Solution
pip install -U topo-descriptors

#### Error
ModuleNotFoundError: No module named 'tensorflow_addons'
#### Solution
https://github.com/onnx/tutorials/issues/201

pip install tensorflow-addons

#### Error
ModuleNotFoundError: No module named 'keras'
#### Solution
https://github.com/keras-team/keras/issues/4889

pip install keras

#### Error
AttributeError: module 'tensorflow.compat.v2.__internal__' has no attribute 'dispatch'
#### Solution
https://stackoverflow.com/questions/67696519/module-tensorflow-compat-v2-internal-has-no-attribute-tf2

pip install -q tensorflow==2.4.3
pip install -q keras==2.4.3
pip install -q tensorflow-estimator==2.4.*

#### Error
ImportError: cannot import name 'tf_utils' from 'keras.utils' (/scratch/fp0/mah900/Miniconda/envs/downscale_dev/lib/python3.8/site-packages/keras/utils/__init__.py)
##### Solutio:
https://stackoverflow.com/questions/57985406/cannot-import-name-tf-utils-when-using-importing-keras

 pip install keras==2.1.5 ### Does not work
 pip install keras==2.6.

#### Error
ModuleNotFoundError: No module named 'tensorflow_probability'
##### solution
Find the Tensorflow supported version:
https://stackoverflow.com/questions/56935876/no-module-named-tensorflow-probability

https://github.com/tensorflow/probability/releases?page=1

pip install -U tensorflow-probability==0.12.2

### No more error  (2022.11.22)

### Error after restart
ImportError: cannot import name 'get_config' from 'tensorflow.python.eager.context' (/scratch/fp0/mah900/Miniconda/envs/downscale_dev/lib/python3.8/site-packages/tensorflow/python/eager/context.py)
#### Solution (Working):
https://stackoverflow.com/a/67708260

    import os 
    os.environ["SM_FRAMEWORK"] = "tf.keras"

### Error again 
### Next, Will try notebook steps directly 

# ***************************************************************************************
# ***************************************************************************************

#### Installation steps from the notebook - working
#### Jupyter Lab missing
https://jupyter.org/install

pip install jupyterlab

### Run from cmd: For data downloading

cd /scratch/fp0/mah900/wind-downscaling-gan
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

conda activate /scratch/fp0/mah900/env/wind_downscale_gan
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

### Port forwarding

PORT=57420
HOST=gadi-login-03
ssh -v -N -L $PORT:$HOST:$PORT gadi.nci.org.au

### Notebook
#### Error
Exception: Client has not agreed to the required terms and conditions.. To access this resource, you first need to accept the termsof 'Licence to use Copernicus Products' at https://cds.climate.copernicus.eu/cdsapp/#!/terms/licence-to-use-copernicus-products
#### Solution:
went to the link and accepted it. Working

#### Error 
subprocess.CalledProcessError: Command 'make -C /home/900/mah900/.cache/elevation/SRTM3 download ENSURE_TILES="srtm_36_02.tif srtm_36_03.tif srtm_36_04.tif srtm_37_02.tif srtm_37_03.tif srtm_37_04.tif srtm_38_02.tif srtm_38_03.tif srtm_38_04.tif"' returned non-zero exit status 2.
#### Solution tried
 cd /home/900/mah900/.cache/elevation/SRTM3

 curl -v -s -o spool/srtm_36_02.zip.temp https://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/srtm_36_02.zip
...
 * SSL certificate problem: certificate has expired
* Closing connection 0

##### use --insecure
https://www.cyberciti.biz/faq/how-to-curl-ignore-ssl-certificate-warnings-command-option/

cd '/home/900/mah900/.cache/elevation/SRTM3'
curl -v --insecure -s -o spool/srtm_36_02.zip.temp https://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/srtm_36_02.zip

mv spool/srtm_36_02.zip.temp spool/srtm_36_02.zip

##### 18, make file : /home/900/mah900/.cache/elevation/SRTM3/Makefile
curl   -s -o $@.temp $(DATASOURCE_URL)/$*$(COMPRESSED_EXT) && mv $@.temp $@
#### Changed to 
curl  --insecure  -s -o $@.temp $(DATASOURCE_URL)/$*$(COMPRESSED_EXT) && mv $@.temp $@
#### Run, directly
make -C /home/900/mah900/.cache/elevation/SRTM3 download ENSURE_TILES="srtm_36_02.tif srtm_36_03.tif srtm_36_04.tif srtm_37_02.tif srtm_37_03.tif srtm_37_04.tif srtm_38_02.tif srtm_38_03.tif srtm_38_04.tif"

$ ls /home/900/mah900/.cache/elevation/SRTM3/cache/
    srtm_36_02.tif       srtm_36_03.tif.lock  srtm_37_02.tif       srtm_37_03.tif.lock  srtm_38_02.tif       srtm_38_03.tif.lock
    srtm_36_02.tif.lock  srtm_36_04.tif       srtm_37_02.tif.lock  srtm_37_04.tif       srtm_38_02.tif.lock  srtm_38_04.tif
    srtm_36_03.tif       srtm_36_04.tif.lock  srtm_37_03.tif       srtm_37_04.tif.lock  srtm_38_03.tif       srtm_38_04.tif.lock


### eio (from conda)

eio --help
Usage: eio [OPTIONS] COMMAND [ARGS]...

Options:
  --version                       Show the version and exit.
  --product [SRTM1|SRTM3|SRTM1_ELLIP]
                                  DEM product choice.  [default: SRTM1]
  --cache_dir DIRECTORY           Root of the DEM cache folder.  [default:
                                  /home/900/mah900/.cache/elevation]

  --help                          Show this message and exit.

Commands:
  clean      Clean up the product cache from temporary files.
  clip       Clip the DEM to given bounds.
  distclean  Remove the product cache entirely.
  info       Show info about the product cache.
  seed       Seed the DEM to given bounds.
  selfcheck  Audit the system for common issues.

#### Test 
https://github.com/bopen/elevation:
eio selfcheck

eio --product SRTM3 clip -o "/scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif"  --bounds -4.96 42.2 8.3 51.3




#### change Make file, line 18, then
make -C /home/900/mah900/.cache/elevation/SRTM3 all
### Error 
 gdalbuildvrt 
 files not supplied, can not read from cache

### (shell) to use shell in the make file
 https://stackoverflow.com/questions/10024279/how-to-use-shell-commands-in-makefile

#### Makefile, line 13
 $(PRODUCT).vrt: $(shell find cache -size +0 -name "*.tif" 2>/dev/null)
#### change to
$(PRODUCT).vrt: $(shell find -name "*.tif" 2>/dev/null)
#### Now run, 
rm cache/*.lock
make -C /home/900/mah900/.cache/elevation/SRTM3 all

### now, got error, can not open file
gdalbuildvrt -q -overwrite SRTM3.vrt cache/srtm_36_02.tif cache/srtm_36_03.tif cache/srtm_36_04.tif cache/srtm_37_02.tif cache/srtm_37_03.tif cache/srtm_37_04.tif cache/srtm_38_02.tif cache/srtm_38_03.tif cache/srtm_38_04.tif
#### Cause all files zero

#### Alternative 
#download all from the site ???
#https://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/
#No need

#curl -s -o spool/srtm_36_02.zip.temp https://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/srtm_36_02.zip && mv spool/srtm_36_02.zip.temp spool/srtm_36_02.zip

##### gdal_translate
https://gdal.org/programs/gdal_translate.html
Converts raster data between different formats.

#### Alternative, use -k
make -C /home/900/mah900/.cache/elevation/SRTM3 download ENSURE_TILES="srtm_36_02.tif srtm_36_03.tif srtm_36_04.tif srtm_37_02.tif srtm_37_03.tif srtm_37_04.tif srtm_38_02.tif srtm_38_03.tif srtm_38_04.tif"

### Next, plan
Made the file read only 

-rw-r--r-- 1 mah900 z00 1666 Nov 23 14:08 Makefile
(/scratch/fp0/mah900/env/wind_downscale_gan) [mah900@gadi-login-02 SRTM3]$ chmod 0444 Makefile 
(/scratch/fp0/mah900/env/wind_downscale_gan) [mah900@gadi-login-02 SRTM3]$ ls -l Makefile 
-r--r--r-- 1 mah900 z00 1666 Nov 23 14:08 Makefile

to catch the fault in the writing location.

 eio --product SRTM3 clip -o "/scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif"  --bounds -4.96 42.2 8.3 51.3
...
  File "/scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/util.py", line 76, in ensure_setup
    with open(path, 'w') as file:
PermissionError: [Errno 13] Permission denied: '/home/900/mah900/.cache/elevation/SRTM3/Makefile'

Next, up

 File "/scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/datasource.py", line 155, in ensure_setup
    util.ensure_setup(datasource_root, product=product, force=force, **spec)

### found Makefile templete in 
 /scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/datasource.mk   
##### backup
cp /scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/datasource.mk  /scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/datasource.mk.backup

##### change 
nano -c /scratch/fp0/mah900/env/wind_downscale_gan/lib/python3.9/site-packages/elevation/datasource.mk
at line 18
        curl -k  -s -o $@.temp $(DATASOURCE_URL)/$*$(COMPRESSED_EXT) && mv $@.temp $@

##### Then
eio --product SRTM3 clip -o "/scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif"  --bounds -4.96 42.2 8.3 51.3        

#### Again, error
...
curl -k  -s -o spool/srtm_38_03.zip.temp https://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/srtm_38_03.zip && mv spool/srtm_38_03.zip.temp spool/srtm_38_03.zip
make: *** [Makefile:18: spool/srtm_38_03.zip] Error 23
...
subprocess.CalledProcessError: Command 'make -C /home/900/mah900/.cache/elevation/SRTM3 download ENSURE_TILES="srtm_36_02.tif srtm_36_03.tif srtm_36_04.tif srtm_37_02.tif srtm_37_03.tif srtm_37_04.tif srtm_38_02.tif srtm_38_03.tif srtm_38_04.tif"' returned non-zero exit status 2.

https://curl.se/mail/archive-2001-11/0097.html
Error 23 is documented to mean:
  "Write error. Curl couldn't write data to a local filesystem or similar."

#### Solution 
cleaned all previous files

#### Error
...
make: *** [Makefile:18: spool/srtm_38_03.zip] Error 23
rm spool/srtm_37_04.tif spool/srtm_36_02.tif spool/srtm_36_03.zip spool/srtm_37_02.zip spool/srtm_36_03.tif spool/srtm_37_03.zip spool/srtm_37_02.tif spool/srtm_38_02.zip spool/srtm_37_03.tif spool/srtm_36_04.zip spool/srtm_38_02.tif spool/srtm_36_04.tif spool/srtm_37_04.zip spool/srtm_36_02.zip
make: Leaving directory '/home/900/mah900/.cache/elevation/SRTM3'
....
#### Solution
https://stackoverflow.com/questions/16703647/why-does-curl-return-error-23-failed-writing-body

This happens when a piped program (e.g. grep) closes the read pipe before the previous program is finished writing the whole page.

change line 18,
        curl -k  -s -o $@.temp $(DATASOURCE_URL)/$*$(COMPRESSED_EXT) | tac | tac && mv $@.temp $@ | wait 

#### Error
ERROR 4: Attempt to create new tiff file `/scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif' failed: No such file or directory
make: *** [Makefile:36: clip] Error 1

##### solution:
mkdir -p /scratch/fp0/mah900/data_tmp/dem/

#### Error
...
gdalbuildvrt -q -overwrite SRTM3.vrt cache/srtm_36_02.tif cache/srtm_36_03.tif cache/srtm_36_04.tif cache/srtm_37_02.tif cache/srtm_37_03.tif cache/srtm_37_04.tif cache/srtm_38_02.tif
ERROR 1: Failed to write .vrt file in FlushCache(bool bAtClosing).
make: *** [Makefile:14: SRTM3.vrt] Error 1
make: *** Deleting file 'SRTM3.vrt'
...
#### Solution
line 14, change to 
        gdalbuildvrt -q -overwrite $@ $^ | tac | tac 




#### Clean run
rm cache/*
rm spool/*
rm SRTM3.*

eio --product SRTM3 clip -o "/scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif"  --bounds -4.96 42.2 8.3 51.3

Not working even after adding tac | tac 

#### Next,  
remove all the rm from the make file and mauallly run 

gdalbuildvrt -q -overwrite SRTM3.vrt cache/srtm_36_02.tif cache/srtm_36_03.tif cache/srtm_36_04.tif cache/srtm_37_02.tif cache/srtm_37_03.tif cache/srtm_37_04.tif cache/srtm_38_02.tif cache/srtm_38_03.tif cache/srtm_38_04.tif

cp SRTM3.vrt SRTM3.8bcd6dca7b0c4ac6ba82ff3ec6d70f62.vrt

gdal_translate -q -co TILED=YES -co COMPRESS=DEFLATE -co ZLEVEL=9 -co PREDICTOR=2 -projwin -4.96 51.3 8.3 42.2 SRTM3.8bcd6dca7b0c4ac6ba82ff3ec6d70f62.vrt /scratch/fp0/mah900/data_tmp/dem/France-90m-DEM.tif  