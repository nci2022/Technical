### Wind downscaling over Switzerland project (EPFL/UNIBE)
https://github.com/OpheliaMiralles/wind-downscaling-gan/blob/master/README.md

#### clone
cd /scratch/fp0/mah900/
git clone  https://github.com/OpheliaMiralles/wind-downscaling-gan.git
#### Edit
nano /scratch/fp0/mah900/wind-downscaling-gan/environment.yml
      - python=3.8.*
    prefix: /scratch/fp0/mah900/env/downscale_dev
#### Install: step1
https://www.tensorflow.org/install/source#gpu
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"

    # 
    #conda env create -f /scratch/fp0/mah900/wind-downscaling-gan/environment.yml
    #Solving environment: failed
    #
    #ResolvePackageNotFound: 
    #  - python[version='3.8.*,3.9.2.*']

    conda create -p /scratch/fp0/mah900/env/wind-downscaling-gan python=3.8
    
#### Install: step2

conda activate /scratch/fp0/mah900/env/wind-downscaling-gan
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

    $ which pip
    /scratch/fp0/mah900/env/wind-downscaling-gan/bin/pip

pip3 install -r requirements.txt

#### Install: step3
pip install topo-descriptors

$ pip uninstall downscaling -y
WARNING: Skipping downscaling as it is not installed.

pip install -U git+https://github.com/OpheliaMiralles/wind-downscaling-gan.git

==>


### Create Account, Get Key
https://cds.climate.copernicus.eu/api-how-to

<!--
url: https://cds.climate.copernicus.eu/api/v2
key: 164095:27f7dc1a-bce4-4d5e-936f-be72557d67c2
-->
nano $HOME/.cdsapirc

#### Packages missing try again

conda install -c conda-forge -c  anaconda \
 python=3.8.* cartopy=0.19.0 elevation=1.1.3 jupyterlab=3.0.14 \
 matplotlib=3.4.1 numpy=1.20.2 pandas=1.2.4 \
 rasterio=1.2.3 scipy=1.6.2 xarray=0.17.0 pip=20.1 \
 tensorboard=2.4.0 tensorflow=2.4.3

### Error
PackagesNotFoundError: The following packages are not available from current channels:

  - python[version='3.8.*,3.9.2.*']

##### Cause 
two Python versions are present in the file  

python=3.8.* 
..
python=3.9.2  

##### Solution
remove one 

### Try 3 (Working)
After conrrecting the "environment" file

cd /scratch/fp0/mah900/wind-downscaling-gan
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

conda env create -f /scratch/fp0/mah900/wind-downscaling-gan/environment.yml

conda activate downscale_dev

##### Next install
conda install -c conda-forge gdal pysftp dask python-dotenv

pip uninstall downscaling -y
pip install -U git+https://github.com/OpheliaMiralles/wind-downscaling-gan.git


### Had error while import

### Next, try jupyter notebook steps directly. 

cd /scratch/fp0/mah900/wind-downscaling-gan
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

conda env create -p /scratch/fp0/mah900/env/wind_downscale python=3.9
conda activate /scratch/fp0/mah900/env/wind_downscale
conda install -y -c conda-forge gdal tensorflow xarray numpy=1.19.5 pandas pysftp cdsapi elevation rasterio dask python-dotenv
pip install topo-descriptors
pip uninstall downscaling -y
pip install -U git+https://github.com/OpheliaMiralles/wind-downscaling-gan.git


##### Test 2
Python 

from datetime import date, datetime
from pathlib import Path
from downscaling.data import download_ERA5
from downscaling import downscale, plot_wind_fields
import xarray as xr
from downscaling import process_era5, process_topo, predict
import requests
from downscaling import plot_elevation, plot_wind_fields
from cartopy.crs import epsg

#### Install part 2. Missing 
pip install silence-tensorflow
pip install parse
pip install -U topo-descriptors
pip install tensorflow-addons

#### tensorflow-addons and Keras
https://github.com/tensorflow/addons/issues/2679

conda install -c conda-forge keras

#### Abort

### New, Try

cd /scratch/fp0/mah900/wind-downscaling-gan
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
module load cuda/11.0.3
module load cudnn/8.1.1-cuda11

###### Conda
=============================================================
conda env create -p /scratch/fp0/mah900/env/wind_downscale_gan

conda install -y -c conda-forge gdal tensorflow xarray numpy=1.19.5 pandas pysftp cdsapi elevation rasterio dask python-dotenv silence-tensorflow parse topo-descriptors tensorflow-addons keras tensorflow-estimator keras tensorflow_probability 

#### Try 1, mamaba
mamba create -p /scratch/fp0/mah900/env/wind_downscale_gan -c conda-forge  -c "conda-forge/label/cf202003" \
gdal tensorflow xarray numpy=1.19.5 pandas pysftp cdsapi elevation rasterio dask python-dotenv silence-tensorflow parse topo-descriptors tensorflow-addons keras tensorflow-estimator keras tensorflow_probability 

#### Try 2, mamaba
mamba create -p /scratch/fp0/mah900/env/wind_downscale_gan -c conda-forge \
gdal tensorflow xarray numpy=1.19.5 pandas pysftp cdsapi elevation rasterio dask python-dotenv silence-tensorflow parse keras tensorflow-estimator keras  

conda activate /scratch/fp0/mah900/env/wind_downscale_gan

pip install topo-descriptors tensorflow-addons tensorflow_probability  

pip install -U git+https://github.com/OpheliaMiralles/wind-downscaling-gan.git
# =====================================================
$ which python
/scratch/fp0/mah900/env/wind_downscale_gan/bin/python

#### Error
ImportError: This version of TensorFlow Probability requires TensorFlow version >= 2.10; Detected an installation of version 2.7.1. Please upgrade TensorFlow to proceed.
#### Solution:
https://github.com/tensorflow/probability/releases:
pip install install -U tensorflow_probability==0.15.0 

https://github.com/tensorflow/addons:
pip install install -U tensorflow_probability==0.14.*

#### Error
ModuleNotFoundError: No module named 'cartopy'

mamba install -c conda-forge cartopy==0.20.3 ## working ( 0.19, 0.21 need reinsall)

