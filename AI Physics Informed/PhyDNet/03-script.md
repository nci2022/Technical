#!/bin/bash

#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=12
#PBS -l ngpus=1
#PBS -l jobfs=200GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=280GB
#PBS -l walltime=48:00:00
#PBS -N Phy_g1_h48
#PBS -P fp0

cd '/scratch/fp0/mah900/PhyDNet'
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/PhyDNet

python main.py \
    --root="/scratch/fp0/mah900/PhyDNet/data" \
    --batch_size=32 \
    --nepochs=1200 \
    --print_every=1 \ 
    --eval_every=10 \
    --save_name="phydnet_32_1200.pth"
