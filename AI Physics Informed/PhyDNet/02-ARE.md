### ARE load

https://are.nci.org.au/

4
gpuvolta
1gpu
fp0
gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

/scratch/fp0/mah900/Miniconda/
/scratch/fp0/mah900/env/PhyDNet

#################################
### File
#scratch/fp0/mah900/PhyDNet/Notebook-01.ipynb
### Updated
scratch/fp0/mah900/PhyDNet/Notebook-02.ipynb

### Test 
import os
os.chdir('/scratch/fp0/mah900/PhyDNet')

import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.optim.lr_scheduler import ReduceLROnPlateau
import numpy as np
import random
import time
from models.models import ConvLSTM,PhyCell, EncoderRNN
from data.moving_mnist import MovingMNIST
from constrain_moments import K2M
# from skimage.measure import compare_ssim as ssim
from skimage.metrics import structural_similarity as ssim
import argparse

################################################

