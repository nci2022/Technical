### PhyDNet Install

cd /scratch/fp0/mah900/
git clone https://github.com/vincent-leguen/PhyDNet.git
cd /scratch/fp0/mah900/PhyDNet


### Test #1
$ module load pytorch/1.10.0
Loading pytorch/1.10.0
  Loading requirement: intel-mkl/2020.3.304 python3/3.9.2 cuda/11.4.1 cudnn/8.2.2-cuda11.4 nccl/2.10.3-cuda11.4 openmpi/4.1.2 magma/2.6.0
    fftw3/3.3.8

$ Python3

import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.optim.lr_scheduler import ReduceLROnPlateau
import numpy as np
import random
import time
from models.models import ConvLSTM,PhyCell, EncoderRNN
from data.moving_mnist import MovingMNIST
from constrain_moments import K2M
from skimage.measure import compare_ssim as ssim
import argparse

### Error 1
>>> import torchvision
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'torchvision'

### Error 2
>>> from skimage.measure import compare_ssim as ssim
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'skimage'

### Matched
#torchvision==0.11.0

#######
### Conda 

. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda create -p /scratch/fp0/mah900/env/PhyDNet python=3.10
conda activate /scratch/fp0/mah900/env/PhyDNet

conda install mamba -n base -c conda-forge
#### Installing mamba into any other environment than base is not supported.

mamba install pytorch==1.12.1 torchvision==0.13.1 torchaudio==0.12.1 cudatoolkit=11.6 scikit-image jupyterlab jupyterlab-spellchecker -c pytorch -c conda-forge

#### Test, repeat

### Error 
>>> from skimage.measure import compare_ssim as ssim
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ImportError: cannot import name 'compare_ssim' from 'skimage.measure' (/scratch/fp0/mah900/env/PhyDNet/lib/python3.10/site-packages/skimage/measure/__init__.py)

#### Solution
https://github.com/williamfzc/stagesepx/issues/150#issuecomment-872447692
Changed in version 0.16: This function was renamed from skimage.measure.compare_ssim to skimage.metrics.structural_similarity.

from  skimage.metrics import structural_similarity as ssim


### Matplotlib
 pip install matplotlib


### Install Holoview
cd /scratch/fp0/mah900/PhyDNet/
. "/scratch/fp0/mah900/Miniconda/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/PhyDNet

##### URL
https://holoviews.org/install.html

conda install -c pyviz holoviews bokeh

#### Error 
ModuleNotFoundError: No module named 'bokeh_catplot'

#### Solution 
#### conda install -c pyviz bokeh-catplot (Does not work)
https://pypi.org/project/bokeh-catplot/
pip install bokeh-catplot

#### Error 
slider not working 

##### Solution
https://ipywidgets.readthedocs.io/en/stable/user_install.html#installing-the-jupyterlab-extension
Tried both 
 pip install ipywidgets
 conda install -c conda-forge ipywidgets (working after this)


### Install tabulate
https://anaconda.org/conda-forge/tabulate
conda install -c conda-forge tabulate 