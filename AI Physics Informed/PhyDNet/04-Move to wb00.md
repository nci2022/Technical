### Move files to wb00 (2023.02.01)

#### ARE - Unchanged
Walltime (hours):1
Queue: gpuvolta
Compute Size: 1gpu
Project: your own project (or fp0 to test).
Storage: gdata/fp0+gdata/dk92+gdata/z00+gdata/wb00

In the Advanced options section: 
Python or Conda virtual environment base: /scratch/fp0/mah900/Miniconda/
Conda environment: /scratch/fp0/mah900/env/PhyDNet

#### Move data

cd /scratch/fp0/mah900/PhyDNet
cp -r constrain_moments.py licence.md data main.py Notebook-02.ipynb save images models __pycache__ /g/data/wb00/admin/staging/PhyDNet

#### Chage notebook

g/data/wb00/admin/staging/PhyDNet/Notebook-02.ipynb

#os.chdir('/scratch/fp0/mah900/PhyDNet')
os.chdir('/g/data/wb00/admin/staging/PhyDNet')

#args_root = "/scratch/fp0/mah900/PhyDNet/data"
args_root = "/g/data/wb00/admin/staging/PhyDNet/data"

#### Permission 
chmod -R 755 .
