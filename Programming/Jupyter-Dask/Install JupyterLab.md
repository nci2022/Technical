### Install JupyterLab

https://jobqueue.dask.org/en/latest/interactive.html


We recommend using JupyterLab, and the Dask JupyterLab Extension. This will make it easy to get Dask’s dashboard through your Jupyter session.

These can be installed with the following steps:

# Install JupyterLab and NodeJS (which we'll need to integrate Dask into JLab)
conda install jupyterlab nodejs -c conda-forge -y

# Install server-side pieces of the Dask-JupyterLab extension
pip install dask_labextension

# Integrate Dask-Labextension with Jupyter (requires NodeJS)
jupyter labextension install dask-labextension
You can also use pip rather than conda, but you will have to find some other way to install NodeJS.

