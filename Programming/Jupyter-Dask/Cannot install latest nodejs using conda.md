### Cannot install latest nodejs using conda

https://stackoverflow.com/questions/62325068/cannot-install-latest-nodejs-using-conda-on-mac


I had a similar problem and this is how I fixed it:

First I did not only conda install -c conda-forge nodejs, but all the commands that are listed in the https://anaconda.org/conda-forge/nodejs.

conda install -c conda-forge nodejs
conda install -c conda-forge/label/gcc7 nodejs
conda install -c conda-forge/label/cf201901 nodejs
conda install -c conda-forge/label/cf202003 nodejs
With this I got 13.10.1 version of nodejs. And after I did conda update nodejs and received 14.8.0 version of it.

https://stackoverflow.com/a/67371745


### packages/tensorflow/python/_pywrap_tensorflow_internal.so: undefined symbol: PyCMethod_New
https://github.com/tensorflow/tensorflow/issues/56247

it seems Tensorflow built from source but from the error, it looks like you are trying to import tensorflow from anaconda library.

### Install TensorFlow with pip
https://www.tensorflow.org/install/pip

### ModuleNotFoundError: No module named 'tensorflow-io' error
https://www.roseindia.net/answers/viewqa/pythonquestions/234762-ModuleNotFoundError-No-module-named-tensorflow-io.html

pip install tensorflow-io


### ModuleNotFoundError: No module named 'xbatcher'
https://pypi.org/project/xbatcher/

pip install xbatcher



