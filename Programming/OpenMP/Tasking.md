### OpenMP	Tasking Explained 
Ruud van der Pas  <br>
https://openmp.org/wp-content/uploads/sc13.tasking.ruud.pdf


### OpenMP Tasking	
https://www.openmp.org/wp-content/uploads/sc15-openmp-CT-MK-tasking.pdf


### #pragma omp task (IBM)
https://www.ibm.com/docs/en/zos/2.4.0?topic=processing-pragma-omp-task


### Chapter 5 Tasking (oracle)
https://docs.oracle.com/cd/E19205-01/820-7883/6nj43o69j/index.html






