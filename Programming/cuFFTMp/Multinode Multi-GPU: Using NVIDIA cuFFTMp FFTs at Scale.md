### Multinode Multi-GPU: Using NVIDIA cuFFTMp FFTs at Scale
https://developer.nvidia.com/blog/multinode-multi-gpu-using-nvidia-cufftmp-ffts-at-scale/

NVIDIA announces the release of cuFFTMp for Early Access (EA). 
cuFFTMp is a multi-node, multi-process extension to cuFFT that enables scientists and engineers to solve challenging problems on exascale platforms.
