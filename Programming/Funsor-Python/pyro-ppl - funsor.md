### pyro-ppl / funsor

https://github.com/pyro-ppl/funsor

https://funsor.pyro.ai/en/stable/index.html

Funsor is a tensor-like library for functions and distributions.

See Functional tensors for probabilistic programming (https://arxiv.org/abs/1910.10775) for a system description.

