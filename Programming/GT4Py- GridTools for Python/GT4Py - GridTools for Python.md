### GT4Py: GridTools for Python

Python library for generating high-performance implementations of stencil kernels for weather and climate modeling from a domain-specific language (DSL).

https://github.com/GridTools/gt4py


https://github.com/GridTools/gt4py/tree/master/examples