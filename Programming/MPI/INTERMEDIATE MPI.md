# INTERMEDIATE MPI

https://enccs.github.io/intermediate-mpi/

This training material targets programmers who already have experience with basic MPI and are ready to take the next step to more advanced usage. Topics covered include communicators and groups, derived datatypes, one-sided communication, collective communication and hybrid MPI-threading approaches. See below for recommended prerequisite knowledge.



### Introducing MPI and threads
https://enccs.github.io/intermediate-mpi/mpi-and-threads-pt1/

