### Mars Lee 

Link to the slides, as Google Slides (https://docs.google.com/presentation/d/1euxQClIGBMHEm4r8fcJUmRLU8sTbrFHCYLICjBPUhis/edit?usp=sharing)

Link to Github repo with PDF and PPTX versin of slides (https://github.com/MarsBarLee/2022-RSE-Asia-Australia-Unconference)

Let's Connect:
https://twitter.com/marsbarlee
https://www.linkedin.com/in/mars-lee/
mlee@quansight.com, email me if you like to book a short meeting with me!


Links

Padlet.com
https://padlet.com/rse2022

Slido.com
1845798
OzAsia